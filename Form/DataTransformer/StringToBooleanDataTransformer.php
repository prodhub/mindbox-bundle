<?php

namespace ADW\MindboxBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StringToBooleanDataTransformer implements DataTransformerInterface
{
    /**
     * @param string $value
     * @return bool
     */
    public function transform($value)
    {
        if (!is_string($value)) {
            throw new TransformationFailedException('Must be string, got ' . gettype($value));
        }

        return $value == 'true';
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        if (!is_bool($value)) {
            throw new TransformationFailedException('Must be boolean, got ' . gettype($value));
        }

        return $value ? 'true' : 'false';
    }
}