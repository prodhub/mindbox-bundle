<?php

namespace ADW\MindboxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PostIndexType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'adw_mindbox__post_index';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}