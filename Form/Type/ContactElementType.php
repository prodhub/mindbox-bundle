<?php

namespace ADW\MindboxBundle\Form\Type;

use ADW\MindboxBundle\Contact\ContactElement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactElementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('id', GeoChoiceType::class, ['label' => false,])
            ->add('name', HiddenType::class)
            ->add('type', ContactTypeType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => ContactElement::class,
            'translation_domain' => 'ADWMindboxBundle',
        ]);
    }
}