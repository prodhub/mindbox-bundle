<?php

namespace ADW\MindboxBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactStreetType extends ContactElementType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('id');
        $builder->add('id', HiddenType::class);

        $builder->remove('name');
        $builder->add('name', null, ['label' => false,]);

        $builder->remove('type');
        $builder->add('type', ContactTypeType::class);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'label_format' => 'form.label.%name%',
            'translation_domain' => 'ADWMindboxBundle',
        ]);
    }
}