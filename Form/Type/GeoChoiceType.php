<?php

namespace ADW\MindboxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GeoChoiceType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'adw_mindbox__geo_choice';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}