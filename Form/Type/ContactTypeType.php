<?php

namespace ADW\MindboxBundle\Form\Type;

use ADW\MindboxBundle\Contact\ContactType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('id', GeoChoiceType::class, ['label' => false,])
            ->add('fullName', HiddenType::class)
            ->add('shortName', HiddenType::class)
            ->add('optimalName', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => ContactType::class,
        ]);
    }
}