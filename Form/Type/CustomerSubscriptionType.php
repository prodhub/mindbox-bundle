<?php

namespace ADW\MindboxBundle\Form\Type;

use ADW\MindboxBundle\Customer\CustomerSubscription;
use ADW\MindboxBundle\Form\DataTransformer\StringToBooleanDataTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerSubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('isActiveForCurrentBrand', CheckboxType::class);

        $builder
            ->get('isActiveForCurrentBrand')
            ->addModelTransformer(new StringToBooleanDataTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'label' => false,
            'data_class' => CustomerSubscription::class,
            'translation_domain' => 'ADWMindboxBundle',
        ]);
    }
}