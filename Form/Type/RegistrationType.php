<?php

namespace ADW\MindboxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('email')
            ->add('mobilePhone')
            ->add('lastName')
            ->add('firstName')
            ->add('middleName')
            ->add('birthday', DateType::class, [
                'years' => range(date('Y'), date('Y') - 100),
            ])
            ->add('postAddress', PostAddressType::class, [
                'label' => false,
                'widget_type' => PostAddressType::TYPE_COMPLEX,
            ])
            ->add('externalIdentities', CollectionType::class, [
                'label' => false,
                'entry_type' => ExternalIdentityType::class,
            ])
            ->add('subscription', CustomerSubscriptionType::class);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'label_format' => 'form.label.%name%',
            'translation_domain' => 'ADWMindboxBundle',
        ]);
    }
}