<?php

namespace ADW\MindboxBundle\Form\Type;

use ADW\MindboxBundle\Contact\PostAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostAddressType extends AbstractType
{
    const TYPE_FULL = 'full';
    const TYPE_COMPLEX = 'complex';
    const TYPE_SIMPLE = 'simple';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('postIndex', PostIndexType::class, ['required' => false,])
            //
            ->add('region', ContactElementType::class, ['required' => false,])
            ->add('district', ContactElementType::class, ['required' => false,])
            ->add('settlement', SettlementElementType::class, ['required' => false,])
            ->add('street', ContactStreetType::class, ['required' => false,])
            //
            ->add('house', null, ['required' => false,])
            ->add('estate', null, ['required' => false,])
            ->add('building', null, ['required' => false,])
            ->add('unit', null, ['required' => false,])
            ->add('flat', null, ['required' => false,])
            //
            ->add('comments', null, ['required' => false,])
            //
            ->add('quickModeInput', TextType::class, [
                'label' => 'form.label.quick_mode_input',
                'required' => ($options['widget_type'] == self::TYPE_SIMPLE),
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'label_format' => 'form.label.%name%',
            'data_class' => PostAddress::class,
            'translation_domain' => 'ADWMindboxBundle',
            'widget_type' => self::TYPE_FULL,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['widget_type'] = $options['widget_type'];
    }

    public function getBlockPrefix()
    {
        return 'adw_mindbox__post_address';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}