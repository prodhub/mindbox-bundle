<?php

namespace ADW\MindboxBundle\Form\Type;

use ADW\MindboxBundle\RestClient\Model\ExternalIdentity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExternalIdentityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('provider', TextType::class);
        $builder->add('value', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'label' => false,
            'data_class' => ExternalIdentity::class,
            'translation_domain' => 'ADWMindboxBundle',
        ]);
    }
}