<?php

namespace ADW\MindboxBundle;

use ADW\MindboxBundle\DependencyInjection\Security\TicketFactory;
use ADW\MindboxBundle\DependencyInjection\Security\UsernamePasswordFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ADWMindboxBundle

 * @package ADW\MindboxBundle
 * @author Artur Vesker
 */
class ADWMindboxBundle extends Bundle
{

    /**
     * @inheritdoc
     */
    public function build(ContainerBuilder $container)
    {
        $extension = $container->getExtension('security');

        $extension->addSecurityListenerFactory(new UsernamePasswordFactory());
        $extension->addSecurityListenerFactory(new TicketFactory());
        $container->addCompilerPass(
            new RegisterListenersPass(
                'adw_mindbox.event_dispatcher',
                'adw_mindbox.event_listener',
                'adw_mindbox.event_subscriber'
            )
        );
    }
}
