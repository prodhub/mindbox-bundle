<?php

namespace ADW\MindboxBundle\Controller;

use ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current\LogonServiceDescription;
use ADW\MindboxBundle\Security\Credentials\UsernamePasswordCredentials;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * Login action
     *
     * @Route("/login", name="adw_mindbox__security__login")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function loginAction(Request $request)
    {
        $model = [
            'login' => null,
            'password' => null,
            'remember' => true,
        ];
        $form = $this->createFormBuilder($model, [
            'intention' => 'post_type',
        ])
            ->add('login')
            ->add('password')
            ->add('remember', CheckboxType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $model = $form->getData();
            /** @var MindboxToken $token */
            $token = $this->getCRM()->request(new LogonServiceDescription(), [
                'credentials' => new UsernamePasswordCredentials(
                    $model['login'],
                    $model['password'],
                    $model['remember']
                ),
            ]);
            $user = $this->getUserProvider()->getCurrentUser($token);
            $token->setUser($user);

            return $this->redirectToRoute('homepage');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Logout action
     *
     * @Route("/logout", name="adw_mindbox__security__logout")
     */
    public function logoutAction()
    {
        throw new \LogicException(
            'Goto security.yml and set you firewall. This page must not be accesible!'
        );
    }

    /**
     * @return \ADW\MindboxBundle\Client\Client|object|\Vesax\RestClientBundle\Client\Client
     */
    private function getCRM()
    {
        return $this->get('adw_mindbox_client');
    }

    /**
     * @return \ADW\MindboxBundle\Security\User\MindboxUserProvider|object
     */
    private function getUserProvider()
    {
        return $this->get('adw_mindbox.security.user_provider');
    }
}