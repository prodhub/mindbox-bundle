<?php

namespace ADW\MindboxBundle\Controller;

use ADW\MindboxBundle\Form\Type\RegistrationType;
use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\RestClient\Model\ExternalIdentity;
use ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current\LogonServiceDescription;
use ADW\MindboxBundle\Security\Credentials\ExternalCredentials;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/social")
 */
class SocialSecurityController extends Controller
{
    /**
     * @Route("/registration/",
     *     name="adw_mindbox__social_security__registration",
     *     requirements={
     *      "provider": "[\w]+",
     *      "uid": "[\d]+",
     *     }
     * )
     *
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function registrationAction(Request $request)
    {
        $customerClass = $this->getParameter('adw_mindbox.config.customer_decorator_class');
        /** @var Customer $model */
        $model = new $customerClass();

        $provider = $request->get('provider');
        $uid = $request->get('uid');

        if (null != $provider && null != $uid) {
            $ei = new ExternalIdentity($provider, $uid);
            $model->setExternalIdentity($ei);
        }
        // 199177108

        $form = $this->createForm(RegistrationType::class, $model, [
            'csrf_token_id' => 'post_type', // need for tests (trick for CSRF)
        ]);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $regOpClass = $this->getParameter('adw_mindbox.config.reg_operation');

            $model->decorateDown();

            /** @var MindboxToken $token */
            $token = $this->getCRM()->request(new $regOpClass(), [
                'data' => $model,
            ]);
            $user = $this->getUserProvider()->getCurrentUser($token);
            $token->setUser($user);

            $referer = $request->headers->get('referer');

            return $this->redirect($referer);
        }

        return array(
            'registration_form' => $form->createView(),
        );
    }

    /**
     * @Route("/login/", name="adw_mindbox__social_security__login")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(Request $request)
    {
        $provider = $request->get('provider');
        $uid = $request->get('uid');

        // Пробуем авторизовать пользователя в соц сети
        $credentials = new ExternalCredentials($provider, $uid);
        $token = $this->getCRM()->request(new LogonServiceDescription(), [
            'credentials' => $credentials,
        ]);
        $customer = $this->getUserProvider()->getCurrentUser($token);
        $token->setUser($customer);

        $referer = $request->headers->get('referer');
        if (!empty($referer)) {
            return $this->redirect($referer);
        }

        return $this->redirect('/');
    }

    /**
     * @return \ADW\MindboxBundle\Security\User\MindboxUserProvider|object
     */
    private function getUserProvider()
    {
        return $this->get('adw_mindbox.security.user_provider');
    }

    /**
     * @return \ADW\MindboxBundle\Client\Client|object
     */
    private function getCRM()
    {
        return $this->get('adw_mindbox_client');
    }
}