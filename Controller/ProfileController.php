<?php

namespace ADW\MindboxBundle\Controller;

use AppBundle\Entity\Customer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/profile")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/edit/", name="adw_mindbox__profile__edit")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @return array
     */
    public function editAction(Request $request)
    {
        $typeClass = $this->getCustomerModelClassName();

        /** @var Customer $model */
        $model = $this->getUser();
        $form = $this->createForm(new $typeClass(), $model, [
            'intention' => 'post_type',
        ]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {

            // Save to CRM
            $this->getCustomerRepo()->save($model);

            // Save to local DB
            // TODO: ловить событие сохранения профиля пользователя и писать копию в локальную БД на событие
            $em = $this->getDoctrine()->getManager();
            $em->merge($model);
            $em->flush();

            return $this->redirectToRoute('adw_mindbox__profile__edit');
        }

        return [
            'edit_form' => $form->createView(),
        ];
    }

    /**
     * @return string
     */
    private function getCustomerModelClassName()
    {
        return $this->getParameter('adw_mindbox.config.customer_edit_type_class');
    }

    /**
     * @return \ADW\MindboxBundle\Repository\CustomerRepository|object
     */
    private function getCustomerRepo()
    {
        return $this->get('adw_mindbox.customer_repository');
    }
}