<?php

namespace ADW\MindboxBundle\Controller;

use ADW\MindboxBundle\RestClient\OperationDescription\DirectCrm\RestorePasswordOperationDescription;
use ADW\MindboxBundle\RestClient\SuccessResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/forgot-password")
 */
class ForgotPasswordController extends Controller
{
    /**
     * @Route("/", name="adw_mindbox__forgot_password__index")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $model = [
            'contact' => null,
        ];
        $form = $this->createFormBuilder($model, [
            'intention' => 'post_type',
        ])
            ->add('contact')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $model = $form->getData();
            /** @var SuccessResponse $res */
            $res = $this->get('adw_mindbox_client')->request(new RestorePasswordOperationDescription(), [
                'contact' => $model['contact'],
            ]);

            foreach ($res->getMessages() as $message) {
                $this->addFlash('forgot-password', $message);
            }

            return $this->redirectToRoute('adw_mindbox__forgot_password__index');
        }

        return [
            'change_pass_form' => $form->createView(),
        ];
    }
}