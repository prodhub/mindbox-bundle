<?php

namespace ADW\MindboxBundle\Operation;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class DeniedOperation
 *
 * @author Artur Vesker
 */
class DeniedOperation extends Operation
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("requiredOperation")
     * @Serialized\XmlAttribute()
     */
    protected $requiredOperation;

    /**
     * @return string
     */
    public function getRequiredOperation()
    {
        return $this->requiredOperation;
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        return false;
    }

}