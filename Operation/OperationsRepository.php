<?php

namespace ADW\MindboxBundle\Operation;

use ADW\MindboxBundle\Exception\OperationNotFoundException;
use ADW\MindboxBundle\RestClient\ServiceDescription\ListOperationsServiceDescription;
use Vesax\RestClientBundle\Client\Client;

/**
 * Class OperationsRepository
 *
 * @author Artur Vesker
 * @author Sergey Cherkesov
 */
class OperationsRepository
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var OperationsList|AllowedOperation[]|DeniedOperation[]
     */
    protected $cachedOperationsList;

    /**
     * AllowanceChecker constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return OperationsList|AllowedOperation[]|DeniedOperation[]
     */
    public function findAll()
    {
        if (!$this->cachedOperationsList) {
            $this->cachedOperationsList =
                $this->client->request(new ListOperationsServiceDescription());
        }

        return $this->cachedOperationsList;
    }

    /**
     * @param $name
     * @return AllowedOperation|DeniedOperation
     */
    public function findByName($name)
    {
        $this->findAll();

        foreach ($this->cachedOperationsList->getAllowed() as $operation) {
            if ($operation->getName() == $name) {
                return $operation;
            }
        }

        foreach ($this->cachedOperationsList->getDenied() as $operation) {
            if ($operation->getName() == $name) {
                return $operation;
            }
        }

        throw new OperationNotFoundException($name, sprintf('Operation %s not found', $name));
    }

    /**
     * Reset operations cache
     */
    public function reset()
    {
        $this->cachedOperationsList = null;
    }

}