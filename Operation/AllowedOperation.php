<?php

namespace ADW\MindboxBundle\Operation;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class AllowedOperation
 *
 * @author Artur Vesker
 */
class AllowedOperation extends Operation
{
    /**
     * @return bool
     */
    public function isAllowed()
    {
        return true;
    }
}