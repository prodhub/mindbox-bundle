<?php

namespace ADW\MindboxBundle\Operation;

use ADW\MindboxBundle\RestClient\Model\Customer;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class PerformData
 *
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("customer")
 * @Serialized\ExclusionPolicy("all")
 */
class PerformData
{

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * PerformData constructor.
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return int
     *
     * @Serialized\VirtualProperty()
     * @Serialized\Type("integer")
     * @Serialized\SerializedName("mindboxId")
     */
    public function getMindboxId()
    {
        return $this->customer->getId();
    }

}