<?php

namespace ADW\MindboxBundle\Operation;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class OperationsList
 *
 * @author Artur Vesker
 *
 * TODO: full operations map
 *
 * @Serialized\XmlRoot("operations")
 */
class OperationsList implements \IteratorAggregate
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\XmlAttribute()
     */
    protected $customerId;

    /**
     * @var AllowedOperation[]
     *
     * @Serialized\Type("array<ADW\MindboxBundle\Operation\AllowedOperation>")
     * @Serialized\XmlList(inline=true, entry="allowedOperation")
     */
    protected $allowedOperations;

    /**
     * @var DeniedOperation[]
     *
     * @Serialized\Type("array<ADW\MindboxBundle\Operation\DeniedOperation>")
     * @Serialized\XmlList(inline=true, entry="deniedOperation")
     */
    protected $deniedOperations;

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return AllowedOperation[]
     */
    public function getAllowed()
    {
        return $this->allowedOperations;
    }

    /**
     * @return DeniedOperation[]
     */
    public function getDenied()
    {
        return $this->deniedOperations;
    }

    /**
     * @return AllowedOperation[]|DeniedOperation[]
     */
    public function getIterator()
    {
        return new \ArrayIterator(array_merge($this->allowedOperations, $this->deniedOperations));
    }

}