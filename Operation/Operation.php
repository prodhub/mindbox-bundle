<?php

namespace ADW\MindboxBundle\Operation;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Operation
 *
 * @author Artur Vesker
 */
class Operation
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

}