<?php

namespace ADW\MindboxBundle\EventListener;

use ADW\MindboxBundle\Exception\OperationNotAllowedException;
use ADW\MindboxBundle\Operation\OperationsRepository;
use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use ADW\MindboxBundle\RestClient\OperationDescription;
use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CachingDescriptionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vesax\RestClientBundle\Event\RequestEvent;
use Vesax\RestClientBundle\Event\ResponseEvent;

/**
 * Check operation allowable and reset operations cache if need
 *
 * @package ADW\MindboxBundle\EventListener
 * @author Artur Vesker
 * @author Sergey Cherkesov
 */
class OperationListener implements EventSubscriberInterface
{

    /**
     * @var OperationsRepository
     */
    protected $operationRepository;

    /**
     * @param OperationsRepository $operationRepository
     */
    public function __construct(OperationsRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::NAME => 'onRequest',
            ResponseEvent::NAME => 'onResponse',
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onRequest(RequestEvent $event)
    {
        $serviceDescription = $event->getMethodDescription();

        if (!$serviceDescription instanceof OperationAwareInterface
            && !$serviceDescription instanceof AbstractOperationDescription
        ) {
            return;
        }

        if ($serviceDescription instanceof OperationDescription) {
            if (!$serviceDescription->isVisible()) {
                return;
            }
        }

        $operationName = $serviceDescription->getOperation($event->getOptions());

        if ($serviceDescription instanceof AbstractMindboxServiceDescription) {
            if ($serviceDescription->bypassOperationsChecker()) {
                $this->operationRepository->reset();
            }
        }

        $operationInfo = $this->operationRepository->findByName($operationName);
        if (null == $operationInfo || !$operationInfo->isAllowed()) {
            throw new OperationNotAllowedException(
                $operationName,
                sprintf('Operation %s not allowed', $operationName)
            );
        }
    }

    /**
     * @param ResponseEvent $event
     */
    public function onResponse(ResponseEvent $event)
    {
        $serviceDescription = $event->getMethodDescription();

        if ($serviceDescription instanceof CachingDescriptionInterface) {
            if ($serviceDescription->resetOperationsCache()) {
                $this->operationRepository->reset();
            }
        }
    }

}