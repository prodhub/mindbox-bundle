<?php

namespace ADW\MindboxBundle\EventListener;

use ADW\MindboxBundle\Exception\OperationNotAllowedException;
use ADW\MindboxBundle\Operation\OperationsRepository;
use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use ADW\MindboxBundle\RestClient\OperationDescription;
use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CachingDescriptionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vesax\RestClientBundle\Event\RequestEvent;
use Vesax\RestClientBundle\Event\ResponseEvent;

/**
 * Check operation allowable and reset operations cache if need
 *
 * @author Sergey Cherkesov
 */
class LogonByPermanentKeySubscriber implements EventSubscriberInterface
{

    /**
     * @var OperationsRepository
     */
    protected $operationRepository;

    /**
     * @param OperationsRepository $operationRepository
     */
    public function __construct(OperationsRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            ResponseEvent::NAME => 'onResponse',
        ];
    }

    /**
     * @param ResponseEvent $event
     */
    public function onResponse(ResponseEvent $event)
    {
        $serviceDescription = $event->getMethodDescription();

        // TODO: realize auth
    }

}