<?php

namespace ADW\MindboxBundle\EventListener;

use Monolog\Logger;
use Vesax\RestClientBundle\Event\RequestEvent;
use Vesax\RestClientBundle\Event\ResponseEvent;

class LoggingListener
{
    /** @var Logger */
    private $fullLogger;

    /**
     * LoggingListener constructor.
     * @param Logger $fullLogger
     */
    public function __construct(Logger $fullLogger)
    {
        $this->fullLogger = $fullLogger;
    }

    public function onResponse(ResponseEvent $responseEvent)
    {
        $responseEvent->getResponse()->getBody()->rewind();

        $this->fullLogger->info(
            sprintf(
                '%s %s | %s | %s == %s',
                $responseEvent->getRequest()->getMethod(),
                $responseEvent->getRequest()->getUri(),
                preg_replace(
                    '/[\s]{2,}/',
                    ' ',
                    print_r($responseEvent->getRequest()->getHeaders(), true)
                ),
                (string) $responseEvent->getRequest()->getBody(),
                $responseEvent->getResponse()->getBody()->getContents()
            )
        );
    }
}