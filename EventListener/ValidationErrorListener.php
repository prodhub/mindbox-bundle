<?php

namespace ADW\MindboxBundle\EventListener;

use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\MindboxBundle\Validation\ConstraintViolationListFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vesax\RestClientBundle\Event\ResponseEvent;

/**
 * Class ValidationErrorListener
 *
 * @package ADW\MindboxBundle\EventListener
 * @author Artur Vesker
 */
class ValidationErrorListener implements EventSubscriberInterface
{

    /**
     * @var ConstraintViolationListFactory
     */
    protected $violationListFactory;

    /**
     * ValidationErrorListener constructor.
     * @param ConstraintViolationListFactory $violationListFactory
     */
    public function __construct(ConstraintViolationListFactory $violationListFactory)
    {
        $this->violationListFactory = $violationListFactory;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            ResponseEvent::NAME => 'onResponse'
        ];
    }

    /**
     * @param ResponseEvent $event
     */
    public function onResponse(ResponseEvent $event)
    {
        //Fucking 200 code of any response
        $responseBody = (string)$event->getResponse()->getBody();

        if (strpos($responseBody, '<validationErrors>')) {
            throw new ValidationViolationException($this->violationListFactory->fromXml($responseBody));
        }
    }

}