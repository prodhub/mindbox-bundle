<?php

namespace ADW\MindboxBundle\Exception;

use ADW\MindboxBundle\Operation\Operation;

/**
 * Class OperationException
 *
 * @package ADW\MindboxBundle\Exception
 * @author Artur Vesker
 */
class OperationException extends \RuntimeException
{

    /**
     * @var Operation|string
     */
    protected $operation;

    public function __construct($operation, $message)
    {
        $this->operation = $operation;
        parent::__construct($message);
    }

    /**
     * @return Operation|string
     */
    public function getOperation()
    {
        return $this->operation;
    }

}