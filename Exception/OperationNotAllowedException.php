<?php

namespace ADW\MindboxBundle\Exception;

/**
 * Class OperationNotAllowedException
 *
 * @package ADW\MindboxBundle\Exception
 * @author Artur Vesker
 */
class OperationNotAllowedException extends OperationException
{

    /**
     * @param string $operation
     * @param string $message
     */
    public function __construct($operation, $message = 'Operation Not Allowed')
    {
        parent::__construct($operation, $message);
    }

}