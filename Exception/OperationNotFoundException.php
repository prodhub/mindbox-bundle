<?php

namespace ADW\MindboxBundle\Exception;

/**
 * Class OperationNotFoundException
 *
 * @package ADW\MindboxBundle\Exception
 * @author Artur Vesker
 */
class OperationNotFoundException extends OperationException
{

    /**
     * @param string $operation
     * @param string $message
     */
    public function __construct($operation, $message = 'Operation Not Found')
    {
        parent::__construct($operation, $message);
    }

}