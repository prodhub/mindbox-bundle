<?php

namespace ADW\MindboxBundle\Validation;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ConstraintViolationListFactory
 *
 * @author Artur Vesker
 */
class ConstraintViolationListFactory
{

    /**
     * @param $xml
     * @param callable|null $locationMap
     * @return null|ConstraintViolationList
     */
    public function fromXml($xml, callable $locationMap = null)
    {
        $decoder = new XmlEncoder('validationErrors');

        $decoded = $decoder->decode($xml, 'array');

        if (!$validationErrors = $this->getValueOrNull($decoded, 'validationError')) {
            return null;
        }

        if (!isset($validationErrors[0])) {
            $validationErrors = [$validationErrors];
        }

        $list = new ConstraintViolationList();

        foreach ($validationErrors as $error) {

            if ($path = $this->getValueOrNull($error, 'location')) {
                if ($locationMap) {
                    if ($resolvedPath = $locationMap($path, $error)) {
                        $path = $resolvedPath;
                    }
                }
            }

            $list->add(new ConstraintViolation(
                $this->getValueOrNull($error, 'message') ?: 'Неизвестная ошибка',
                null,
                [],
                null,
                $path,
                null
            ));
        }

        return $list;
    }

    /**
     * @param array $data
     * @param $key
     * @return null
     */
    protected function getValueOrNull(array $data, $key)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return null;
    }

}