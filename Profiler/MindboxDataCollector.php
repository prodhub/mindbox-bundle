<?php

namespace ADW\MindboxBundle\Profiler;

use ADW\MindboxBundle\Operation\OperationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Class MindboxDataCollector
 *
 * @package ADW\MindboxBundle\Profiler
 * @author Artur Vesker
 */
class MindboxDataCollector extends DataCollector
{

    /**
     * @var OperationsRepository
     */
    protected $operationRepository;

    /**
     * @param OperationsRepository $operationRepository
     */
    public function __construct(OperationsRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }

    /**
     * @inheritdoc
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $operationsList = $this->operationRepository->findAll();

        $this->data['allowed'] = $operationsList->getAllowed();
        $this->data['customer_id'] = $operationsList->getCustomerId();
        $this->data['denied'] = $operationsList->getDenied();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'mindbox';
    }

    /**
     * @inheritdoc
     */
    public function getCustomer()
    {
        return $this->data['customer_id'];
    }

    /**
     * @inheritdoc
     */
    public function getAllowedOperations()
    {
        return $this->data['allowed'];
    }

    /**
     * @inheritdoc
     */
    public function getDeniedOperations()
    {
        return $this->data['denied'];
    }

}