<?php
/**
 * Created by PhpStorm.
 * User: scherk01
 * Date: 08.04.2016
 * Time: 18:21
 */

namespace ADW\MindboxBundle\RestClient\DataModel;


/**
 * Class StandardServiceResponseModel
 * @package ADW\MindboxBundle\RestClient\DataModel
 */
class StandardServiceResponseModel
{
    /**
     * @var array
     */
    protected $messages = array();
}