<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

class CustomersServiceDescription
    extends AbstractMindboxServiceDescription
{

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        // TODO: Implement getResponseDataModel() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }
}