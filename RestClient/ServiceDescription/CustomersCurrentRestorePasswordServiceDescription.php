<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

class CustomersCurrentRestorePasswordServiceDescription extends AbstractMindboxServiceDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers/current/restore-password';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @inheritDoc
     */
    public function getOptions()
    {
        return [
            'contact' => 'string',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        return $options;
    }
}