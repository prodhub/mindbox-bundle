<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

use ADW\MindboxBundle\RestClient\Model\Customer;

class CustomerCurrentServiceDescription extends AbstractMindboxServiceDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return Customer::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers/current';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }
}