<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

/**
 * Class ListOperationsServiceDescription
 *
 * @author Artur Vesker
 */
class ListOperationsServiceDescription extends AbstractMindboxServiceDescription
{

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'ADW\MindboxBundle\Operation\OperationsList';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/current/available-operations';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }


}