<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;

/**
 * Class PerformOperationServiceDescription
 *
 * @author Artur Vesker
 */
class PerformOperationServiceDescription extends AbstractMindboxServiceDescription
{

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'array';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/perform-operation';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'POST';
    }

}