<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;

/**
 * Class ProcessTicketServiceDescription
 * @package ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current
 */
class ProcessTicketServiceDescription
    extends AbstractMindboxServiceDescription
{
    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'ticket' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\MindboxBundle\RestClient\DataModel\StandardServiceResponseModel';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers/current/ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }
}