<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use Vesax\RestClientBundle\Event\RequestEvent;

/**
 * Class PutCustomerMethodDescription
 *
 * @author Anton Prokhorov
 */
class PutCustomerMethodDescription extends AbstractMindboxServiceDescription implements OperationAwareInterface
{

    /**
     * @var string
     */
    protected $userClass;

    /**
     * PutCustomerMethodDescription constructor.
     *
     * @param string $userClass
     */
    public function __construct($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'data' => 'string'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return $this->userClass;
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/current';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'PUT';
    }

    public function getOperation(array $options)
    {
        return $options['data'];
    }


}