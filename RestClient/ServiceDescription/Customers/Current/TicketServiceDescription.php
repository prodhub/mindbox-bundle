<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;

/**
 * Class TicketServiceDescription
 *
 * @package ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current
 * @author Artur Vesker
 */
class TicketServiceDescription extends AbstractMindboxServiceDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'ticket' => 'string',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'ADW\MindboxBundle\Security\Token\MindboxToken';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/current/ticket';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        return $options;
    }

    /**
     * @return string
     */
    public function getAuthorizationType()
    {
        return self::AUTH_TYPE_ANONYMOUS;
    }
}
