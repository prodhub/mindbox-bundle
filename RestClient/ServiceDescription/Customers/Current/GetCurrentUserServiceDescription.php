<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use Vesax\RestClientBundle\Event\RequestEvent;

/**
 * Class GetCurrentUserServiceDescription
 *
 * @author Artur Vesker
 */
class GetCurrentUserServiceDescription extends AbstractMindboxServiceDescription implements OperationAwareInterface
{

    /**
     * @var string
     */
    protected $userClass;

    /**
     * GetCurrentUserServiceDescription constructor.
     *
     * @param string $userClass
     */
    public function __construct($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'operation' => 'string'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return $this->userClass;
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/current';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }

    public function getOperation(array $options)
    {
        return $options['operation'];
    }


}