<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current;

use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\Security\Token\MindboxToken;

/**
 * Logon by username and password
 *
 * @author Artur Vesker
 */
class LogonServiceDescription extends AbstractMindboxServiceDescription implements OperationAwareInterface
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'credentials' => 'ADW\MindboxBundle\Security\Credentials\AbstractMindboxCredentials',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return MindboxToken::class;
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/v2/customers/current/logon';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
       return 'POST';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options['credentials'];
    }

    /**
     * @inheritdoc
     */
    public function getOperation(array $options)
    {
        return $options['credentials']->getOperation();
    }

}