<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription\Customers;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;

/**
 * Customers registration service
 *
 * Class CustomersServiceDescription
 * @package ADW\MindboxBundle\RestClient\ServiceDescription\Customers
 */
class CustomersServiceDescription
    extends AbstractMindboxServiceDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        // TODO: Implement getResponseDataModel() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }
}