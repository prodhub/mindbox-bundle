<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

class ServiceDescription extends AbstractMindboxServiceDescription
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $method;

    /**
     * ServiceDescription constructor.
     * @param $model
     * @param $path
     * @param $method
     */
    public function __construct($model, $path, $method)
    {
        $this->model = $model;
        $this->path = $path;
        $this->method = $method;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return $this->model;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->method;
    }
}