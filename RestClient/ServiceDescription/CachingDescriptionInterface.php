<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

/**
 * Interface CachingDescriptionInterface
 *
 * @package ADW\MindboxBundle\RestClient\ServiceDescription
 */
interface CachingDescriptionInterface
{

    /**
     * @return bool
     */
    public function resetOperationsCache();

    /**
     * @return int
     */
    public function getCacheTtl();

}