<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

use ADW\MindboxBundle\RestClient\Model\LogonViaCookie;
use ADW\MindboxBundle\Security\Token\MindboxToken;

class CustomersCurrentLogonViaCookieServiceDescription
    extends AbstractMindboxServiceDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return MindboxToken::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
//        return '/v2/customers/current/logon/via-cookie';
        return '/v2/customers/current/logon-via-cookie';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }

    /**
     * @inheritDoc
     */
    public function getOptions()
    {
        return [
            'customerId' => 'string',
            'key' => 'string',
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRequestData(array $options)
    {
//        return null;
        return (new LogonViaCookie())
            ->setCustomerId($options['customerId'])
            ->setKey($options['key']);
    }

    /**
     * @inheritDoc
     */
    public function __getQuery(array $options)
    {
        return array_merge([
            'id' => $options['customerId'],
            'key' => $options['key'],
        ], parent::getQuery($options));
    }

    /**
     * @inheritDoc
     */
    public function getAuthorizationType()
    {
        return self::AUTH_TYPE_ANONYMOUS;
    }
}