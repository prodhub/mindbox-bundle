<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

use ADW\MindboxBundle\RestClient\OperationAwareInterface;
use Vesax\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class AbstractMindboxServiceDescription
 *
 * @author Artur Vesker
 */
abstract class AbstractMindboxServiceDescription
    implements ServiceDescriptionInterface, CachingDescriptionInterface
{
    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_PUT = 'PUT';
    const HTTP_METHOD_PATCH = 'PATCH';
    const HTTP_METHOD_DELETE = 'DELETE';

    /**
     * @inheritdoc
     */
    public function getResponseDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_XML;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        if ($this instanceof OperationAwareInterface) {
            return [
                'operation' => $this->getOperation($options),
            ];
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getRequestDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_XML;
    }

    /**
     * @inheritdoc
     */
    public function getRequestDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function bypassOperationsChecker()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function resetOperationsCache()
    {
        return in_array($this->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE']);
    }

    /**
     * @inheritdoc
     */
    public function getCacheTtl()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getAuthorizationType()
    {
        return self::AUTH_TYPE_CUSTOMER;
    }
}