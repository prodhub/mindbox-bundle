<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

use ADW\MindboxBundle\RestClient\Model\Balance\BalanceHistory;

/**
 * @link https://mindbox.fogbugz.com/default.asp?W752
 */
class CustomersCurrentBalanceHistoryServiceDescription
    extends AbstractMindboxServiceDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return BalanceHistory::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/v2/customers/current/balance-history';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'startDateTime' => '\DateTime',
            'endDateTime' => '\DateTime',
            'orderBy' => 'array',
            'startingIndex' => 'integer',
            'countToReturn' => 'integer',
        ];
    }
}