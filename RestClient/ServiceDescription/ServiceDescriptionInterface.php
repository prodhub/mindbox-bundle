<?php

namespace ADW\MindboxBundle\RestClient\ServiceDescription;

use Vesax\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Interface ServiceDescriptionInterface
 * @package ADW\MindboxBundle\RestClient\ServiceDescription
 */
interface ServiceDescriptionInterface extends MethodDescriptionInterface
{
    const AUTH_TYPE_CUSTOMER = 'CUSTOMER';
    const AUTH_TYPE_ANONYMOUS = 'ANONYMOUS';
    const AUTH_TYPE_STAFF = 'STAFF';

    /**
     * @return string
     */
    public function getAuthorizationType();
}