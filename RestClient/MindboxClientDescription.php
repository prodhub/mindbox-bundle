<?php

namespace ADW\MindboxBundle\RestClient;

use ADW\MindboxBundle\RestClient\ServiceDescription\ServiceDescriptionInterface;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use ADW\MindboxBundle\Utils\RequestUtils;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Vesax\RestClientBundle\Client\ClientDescriptionInterface;
use Vesax\RestClientBundle\Event\RequestEvent;

/**
 * Class MindboxClientDescription
 *
 * @author Artur Vesker
 */
class MindboxClientDescription implements ClientDescriptionInterface
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $brand;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var string
     */
    protected $staffLogin;
    /**
     * @var string
     */
    protected $staffPassword;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * MindboxClientDescription constructor.
     *
     * @param string $host
     * @param string $key
     * @param string $brand
     * @param string $channel
     * @param string $staffLogin
     * @param string $staffPassword
     * @param Serializer $serializer
     * @param TokenStorageInterface $tokenStorage
     * @param RequestStack $requestStack
     */
    public function __construct(
        $host,
        $key,
        $brand,
        $channel,
        $staffLogin,
        $staffPassword,
        Serializer $serializer,
        TokenStorageInterface $tokenStorage,
        RequestStack $requestStack
    ) {
        $this->host = $host;
        $this->key = $key;
        $this->brand = $brand;
        $this->channel = $channel;
        $this->staffLogin = $staffLogin;
        $this->staffPassword = $staffPassword;
        $this->serializer = $serializer;
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @inheritdoc
     */
    public function getSchema()
    {
        return 'https';
    }

    /**
     * @return callable
     */
    public function getSerializer()
    {
        return function ($data, $format, array $context) {
            $jmsContext = SerializationContext::create();

            $jmsContext->setSerializeNull(true);

            if (array_key_exists('serialize_null', $context)) {
                $jmsContext->setSerializeNull($context['serialize_null']);
                unset($context['serialize_null']);
            }

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            return $this->serializer->serialize($data, $format, $jmsContext);
        };
    }

    /**
     * @return callable
     */
    public function getDeserializer()
    {
        return function ($description, $data, $format, $model, array $context) {
            $jmsContext = DeserializationContext::create();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('_context', clone $jmsContext);

            return $this->serializer->deserialize($data, $model, $format, $jmsContext);
        };
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => [$this, 'prepareRequest'],
            ],
        ];

    }

    /**
     * @param RequestEvent $event
     */
    public function prepareRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $userRequest = $this->requestStack->getMasterRequest();

        $modify = [];

        $sessionKey = null;
        $customerId = null;

        if ($token = $this->tokenStorage->getToken()) {
            if ($token instanceof MindboxToken) {
                $customerId = $token->getCustomerId();
                $sessionKey = $token->getSessionKey();
            }
        }

        /** @var ServiceDescriptionInterface $description */
        $description = $event->getMethodDescription();

        $modify['set_headers'] = [
            'X-Customer-IP' => $userRequest->getClientIp(),
            'X-Customer-URI' => $userRequest->getUri(),
            'X-Customer-Agent' => $userRequest->headers->get('User-Agent'),

            'Accept' => 'application/xml',
            'Authorization' => RequestUtils::createAuthorizationHeader(
                $this->key, $customerId, $sessionKey,
                $description->getAuthorizationType(),
                $this->staffLogin,
                $this->staffPassword
            ),

            'X-Brand' => $this->brand,
            'X-Point-Of-Contact' => $this->channel,
            'X-Channel' => $this->channel,
            'X-Customer-RefererURI' => $userRequest->request->get('Referer'),
            'X-Customer-Session' => $userRequest->cookies->get('directCrm-session'),
            'Content-Type' => 'text/xml; charset=utf-8',
        ];

        if ($description instanceof OperationDescription) {
            if ($description->isStaffOperation()) {
                $modify['set_headers']['Authorization'] = 'DirectCrm key="' . $this->key . '"';
            }
        }

        $event->setRequest(\GuzzleHttp\Psr7\modify_request($request, $modify));
    }


}