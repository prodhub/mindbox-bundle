<?php

namespace ADW\MindboxBundle\RestClient\OperationDescription;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CachingDescriptionInterface;
use ADW\MindboxBundle\RestClient\ServiceDescription\ServiceDescriptionInterface;

abstract class AbstractOperationDescription
    implements ServiceDescriptionInterface, CachingDescriptionInterface
{
    /**
     * @return AbstractMindboxServiceDescription
     */
    public abstract function getServiceDescription();

    /**
     * @return string
     */
    public abstract function getOperation();

    /**
     * {@inheritdoc}
     */
    public function getResponseDataFormat()
    {
        return $this->getServiceDescription()->getResponseDataFormat();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return $this->getServiceDescription()->getResponseDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataContext()
    {
        return $this->getServiceDescription()->getResponseDataContext();
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return $this->getServiceDescription()->getOptions();
    }

    /**
     * {@inheritdoc}
     */
    final public function getResource()
    {
        return $this->getServiceDescription()->getResource();
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return array_merge(
            $this->getServiceDescription()->getQuery($options),
            [
                'operation' => $this->getOperation(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->getServiceDescription()->getMethod();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $this->getServiceDescription()->getRequestData($options);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataFormat()
    {
        return $this->getServiceDescription()->getRequestDataFormat();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataContext()
    {
        return $this->getServiceDescription()->getRequestDataContext();
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return $this->getServiceDescription()->getSubscribedEvents();
    }

    /**
     * @return bool
     */
    public function resetOperationsCache()
    {
        return $this->getServiceDescription()->resetOperationsCache();
    }

    /**
     * @return int
     */
    public function getCacheTtl()
    {
        return $this->getServiceDescription()->getCacheTtl();
    }

    /**
     * @return string
     */
    public function getAuthorizationType()
    {
        return $this->getServiceDescription()->getAuthorizationType();
    }
}