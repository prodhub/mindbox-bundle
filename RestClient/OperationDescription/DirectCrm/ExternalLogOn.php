<?php

namespace ADW\MindboxBundle\RestClient\OperationDescription\DirectCrm;

use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current\LogonServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CustomersCurrentRestorePasswordServiceDescription;
use ADW\MindboxBundle\RestClient\SuccessResponse;
use ADW\MindboxBundle\Security\Token\MindboxToken;

class ExternalLogOn extends AbstractOperationDescription
{
    /**
     * @return AbstractMindboxServiceDescription
     */
    public function getServiceDescription()
    {
        return new LogonServiceDescription();
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return 'DirectCrm.ExternalLogOn';
    }

    /**
     * @inheritDoc
     */
    public function getResponseDataModel()
    {
        return MindboxToken::class;
    }
}