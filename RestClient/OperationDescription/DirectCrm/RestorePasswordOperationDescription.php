<?php

namespace ADW\MindboxBundle\RestClient\OperationDescription\DirectCrm;

use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CustomersCurrentRestorePasswordServiceDescription;
use ADW\MindboxBundle\RestClient\SuccessResponse;

class RestorePasswordOperationDescription extends AbstractOperationDescription
{
    /**
     * @return AbstractMindboxServiceDescription
     */
    public function getServiceDescription()
    {
        return new CustomersCurrentRestorePasswordServiceDescription();
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return 'DirectCrm.RestorePassword';
    }

    /**
     * @inheritDoc
     */
    public function getResponseDataModel()
    {
        return SuccessResponse::class;
    }
}