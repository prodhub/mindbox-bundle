<?php

namespace ADW\MindboxBundle\RestClient;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SuccessResponse
 *
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("success")
 */
class SuccessResponse
{

    /**
     * @var MessageList
     *
     * @Serialized\Type("ADW\MindboxBundle\RestClient\MessageList")
     * @Serialized\SerializedName("messages")
     */
    protected $messages;

    /**
     * @return MessageList
     */
    public function getMessages()
    {
        return $this->messages;
    }
}