<?php

namespace ADW\MindboxBundle\RestClient;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Message
 *
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("messages")
 */
class MessageList implements \IteratorAggregate, \JsonSerializable, \ArrayAccess
{

    /**
     * @var string[]
     *
     * @Serialized\Type("array<string>")
     * @Serialized\XmlList(entry="message", inline=true)
     */
    protected $messages;

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->messages);
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return implode("\n", $this->messages);
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->messages;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return isset($this->messages[$offset]);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->messages[$offset];
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        $this->messages[$offset] = $value;
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        unset($this->messages[$offset]);
    }

}