<?php

namespace ADW\MindboxBundle\RestClient\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * @Serialized\XmlRoot("externalIdentity")
 */
class ExternalIdentity
{
    const PROVIDER_VK = 'vkontakte';
    const PROVIDER_FB = 'facebook';
    const PROVIDER_OK = 'odnoklassniki';

    /**
     * @var string
     *
     * @Serialized\XmlAttribute()
     * @Serialized\Type("string")
     * @Serialized\Groups({"get", "save"})
     */
    protected $provider;

    /**
     * @var integer
     *
     * @Serialized\XmlAttribute()
     * @Serialized\Type("integer")
     * @Serialized\Groups({"get", "save"})
     */
    protected $value;

    /**
     * ExternalIdentity constructor.
     * @param string $provider
     * @param int $value
     */
    public function __construct($provider = null, $value = null)
    {
        $this->provider = $provider;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     * @return ExternalIdentity
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return ExternalIdentity
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}