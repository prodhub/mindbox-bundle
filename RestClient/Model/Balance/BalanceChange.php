<?php

namespace ADW\MindboxBundle\RestClient\Model\Balance;

use ADW\MindboxBundle\Customer\CustomerBirthDay;

/**
 * @Serialized\XmlRoot("balanceChange")
 */
class BalanceChange
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $changeAmount;

    /**
     * @var CustomerBirthDay
     *
     * @Serialized\Type("ADW\MindboxBundle\Customer\CustomerBirthDay")
     */
    protected $dateChanged;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $comment;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $additionalComment;

    /**
     * @return string
     */
    public function getChangeAmount()
    {
        return $this->changeAmount;
    }

    /**
     * @param string $changeAmount
     * @return $this
     */
    public function setChangeAmount($changeAmount)
    {
        $this->changeAmount = $changeAmount;

        return $this;
    }

    /**
     * @return CustomerBirthDay
     */
    public function getDateChanged()
    {
        return $this->dateChanged;
    }

    /**
     * @param CustomerBirthDay $dateChanged
     * @return $this
     */
    public function setDateChanged($dateChanged)
    {
        $this->dateChanged = $dateChanged;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalComment()
    {
        return $this->additionalComment;
    }

    /**
     * @param string $additionalComment
     * @return $this
     */
    public function setAdditionalComment($additionalComment)
    {
        $this->additionalComment = $additionalComment;

        return $this;
    }
}