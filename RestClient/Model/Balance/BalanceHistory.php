<?php

namespace ADW\MindboxBundle\RestClient\Model\Balance;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialized;

/**
 * @Serialized\XmlRoot("list")
 */
class BalanceHistory
{
    /**
     * @var integer
     *
     * @Serialized\XmlAttribute()
     */
    private $totalCount;

    /**
     * @var BalanceChange[]|ArrayCollection
     *
     * @Serialized\Type("ArrayCollection<ADW\MindboxBundle\RestClient\Model\Balance\BalanceChange>")
     * @Serialized\XmlList(inline=true)
     */
    private $balanceChanges;

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;

        return $this;
    }

    /**
     * @return BalanceChange[]|ArrayCollection
     */
    public function getBalanceChanges()
    {
        return $this->balanceChanges;
    }

    /**
     * @param BalanceChange[]|ArrayCollection $balanceChanges
     * @return $this
     */
    public function setBalanceChanges($balanceChanges)
    {
        $this->balanceChanges = $balanceChanges;

        return $this;
    }
}