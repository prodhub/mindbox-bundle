<?php

namespace ADW\MindboxBundle\RestClient\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * @Serialized\XmlRoot("credentials")
 */
class LogonViaCookie
{
    /**
     * @var integer
     *
     * @Serialized\SerializedName("customerId")
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    private $customerId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    private $key;

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return LogonViaCookie
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return LogonViaCookie
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }
}