<?php

namespace ADW\MindboxBundle\RestClient\Model;

use ADW\MindboxBundle\Contact\PostAddress;
use ADW\MindboxBundle\Customer\CustomerBirthDay;
use ADW\MindboxBundle\Customer\CustomerName;
use ADW\MindboxBundle\Customer\CustomerSubscription;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialized;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Serialized\XmlRoot("customer")
 */
class Customer implements UserInterface
{
    /**
     * @var integer
     *
     * @Serialized\XmlAttribute()
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("version")
     * @Serialized\XmlElement(cdata=false)
     * @Serialized\Groups({"get", "save"})
     */
    protected $version;

    /**
     * @var \ADW\MindboxBundle\Customer\CustomerName
     *
     * @Serialized\Type("ADW\MindboxBundle\Customer\CustomerName")
     * @Serialized\SerializedName("name")
     * @Serialized\Groups({"get", "save"})
     */
    protected $mbName;

    /**
     * @var string
     *
     * @Serialized\XmlElement(cdata=false)
     * @Serialized\Type("string")
     */
    protected $lastName;

    /**
     * @var string
     *
     * @Serialized\XmlElement(cdata=false)
     * @Serialized\Type("string")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Serialized\XmlElement(cdata=false)
     * @Serialized\Type("string")
     */
    protected $middleName;

    /**
     * @var \ADW\MindboxBundle\Customer\CustomerBirthDay
     *
     * @Serialized\Type("ADW\MindboxBundle\Customer\CustomerBirthDay")
     * @Serialized\SerializedName("birthdate")
     * @Serialized\Groups({"get", "save"})
     */
    protected $mbBirthday;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $email;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $mobilePhone;

    /**
     * @var ExternalIdentity[]
     *
     * @Serialized\Type("ArrayCollection<ADW\MindboxBundle\RestClient\Model\ExternalIdentity>")
     * @Serialized\SerializedName("externalIdentities")
     * @Serialized\Groups({"get", "save"})
     * @Serialized\XmlList(entry="externalIdentity")
     */
    protected $externalIdentities;

    /**
     * @var PostAddress
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\PostAddress")
     * @Serialized\SerializedName("postAddress")
     * @Serialized\Groups({"get", "save"})
     */
    protected $postAddress;

    /**
     * @var \ADW\MindboxBundle\Customer\CustomerSubscription
     *
     * @Serialized\Type("ADW\MindboxBundle\Customer\CustomerSubscription")
     * @Serialized\SerializedName("subscription")
     * @Serialized\Groups({"get", "save"})
     */
    protected $subscription;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->mbName = new CustomerName();
        $this->mbBirthday = new CustomerBirthDay();
        $this->externalIdentities = new ArrayCollection();
        $this->subscription = new CustomerSubscription();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return \ADW\MindboxBundle\Customer\CustomerName
     */
    public function getMbName()
    {
        return $this->mbName;
    }

    /**
     * @param \ADW\MindboxBundle\Customer\CustomerName $mbName
     * @return Customer
     */
    public function setMbName($mbName)
    {
        $this->mbName = $mbName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Customer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return Customer
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return \ADW\MindboxBundle\Customer\CustomerBirthDay
     */
    public function getMbBirthday()
    {
        return $this->mbBirthday;
    }

    /**
     * @param \ADW\MindboxBundle\Customer\CustomerBirthDay $mbBirthday
     * @return Customer
     */
    public function setMbBirthday($mbBirthday)
    {
        $this->mbBirthday = $mbBirthday;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param string $mobilePhone
     * @return $this
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * @return array
     */
    public function getExternalIdentities()
    {
        if (is_array($this->externalIdentities)) {
            $this->externalIdentities = new ArrayCollection($this->externalIdentities);
        }

        return $this->externalIdentities;
    }

    /**
     * @param array $externalIdentities
     * @return $this
     */
    public function setExternalIdentities($externalIdentities)
    {
        $this->externalIdentities = $externalIdentities;

        return $this;
    }

    /**
     * @param string $provider
     * @return ExternalIdentity|null
     */
    public function getExternalIdentity($provider)
    {
        if (null != $this->externalIdentities) {
            foreach ($this->externalIdentities as $ei) {
                if ($ei->getProvider() === $provider) {
                    return $ei;
                }
            }
        }

        return null;
    }

    /**
     * @param ExternalIdentity $identity
     * @return $this
     */
    public function setExternalIdentity(ExternalIdentity $identity)
    {
        if (null != $this->externalIdentities) {
            foreach ($this->externalIdentities as $ei) {
                if ($ei->getProvider() === $identity->getProvider()) {
                    $this->externalIdentities->removeElement($ei);
                    break;
                }
            }
        } else {
            $this->externalIdentities = new ArrayCollection();
        }

        $this->externalIdentities->add($identity);

        return $this;
    }

    /**
     * @return PostAddress
     */
    public function getPostAddress()
    {
        return $this->postAddress;
    }

    /**
     * @param PostAddress $postAddress
     * @return $this
     */
    public function setPostAddress($postAddress)
    {
        $this->postAddress = $postAddress;

        return $this;
    }

    /**
     * @return \ADW\MindboxBundle\Customer\CustomerSubscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param \ADW\MindboxBundle\Customer\CustomerSubscription $subscription
     * @return $this
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Данные, поступающие из CRM, приходят в неудобном для хранения в БД формате.
     * Наследуйте этот класс, опишите поля для хранения записей в БД, переопредите данный метод.
     * Опишите по какому принципу переносить данные из "неудобных" полей в "удобные".
     * Например, name->getLast() => lastName
     *
     * @return void
     */
    public function decorateUp()
    {
    }

    /**
     * Тут опишите обратный процесс - перенос из кастомных полей в "неудобные"
     */
    public function decorateDown()
    {
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function decorate(Customer $customer)
    {
        if ($this->id != null && $this->id != $customer->getId()) {
            throw new \LogicException('You can\'n merge data from different accounts!');
        }

        $this
            ->setId($customer->getId())
            ->setVersion($customer->getVersion())
            ->setMbName($customer->getMbName())
            ->setMbBirthday($customer->getMbBirthday())
            ->setEmail($customer->getEmail())
            ->setMobilePhone($customer->getMobilePhone())
            ->setExternalIdentities($customer->getExternalIdentities())
            ->setPostAddress($customer->getPostAddress())
            ->setSubscription($customer->getSubscription());

        $this->decorateUp();

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        if ($this->getEmail()) {
            return $this->getEmail();
        }

        if ($this->getMobilePhone()) {
            return $this->getMobilePhone();
        }

        if ($this->getId()) {
            return $this->getId();
        }

        return 'undefined';
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
        return;
    }
}