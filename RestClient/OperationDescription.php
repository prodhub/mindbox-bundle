<?php

namespace ADW\MindboxBundle\RestClient;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use Vesax\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class OperationDescription
 *
 * @package ADW\MindboxBundle\RestClient
 * @author Artur Vesker
 */
class OperationDescription extends AbstractMindboxServiceDescription implements OperationAwareInterface
{

    /**
     * @var MethodDescriptionInterface
     */
    protected $service;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $isStaffOperation;

    /**
     * OperationDescription constructor.
     *
     * @param MethodDescriptionInterface $service
     * @param string $name
     * @param string $method
     * @param string $model
     * @param array $options
     * @param bool $isStaffOperation
     */
    public function __construct(MethodDescriptionInterface $service, $name, $method, $model, array $options = [], $isStaffOperation = false)
    {
        $this->service = $service;
        $this->name = $name;
        $this->method = $method;
        $this->model = $model;
        $this->options = $options;
        $this->isStaffOperation = $isStaffOperation;
    }

    public function getResponseDataModel()
    {
        return $this->model;
    }

    final public function getResource()
    {
        return $this->service->getResource();
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getOperation(array $options)
    {
        return $this->name;
    }

    public function isVisible()
    {
        return false;
    }

    public function isStaffOperation()
    {
        return $this->isStaffOperation;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getRequestData(array $options)
    {

        if (array_key_exists('data', $options)) {
            return $options['data'];
        }

        return parent::getRequestData($options);
    }


}