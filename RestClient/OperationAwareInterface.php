<?php

namespace ADW\MindboxBundle\RestClient;

/**
 * Interface OperationAwareInterface
 *
 * @package ADW\MindboxBundle\RestClient
 */
interface OperationAwareInterface
{

    /**
     * @param array $options
     * @return string
     */
    public function getOperation(array $options);

}