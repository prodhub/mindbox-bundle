<?php

namespace ADW\MindboxBundle\Customer;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class CustomerDecorator
 *
 * @package ADW\MindboxBundle\Customer
 * @author Artur Vesker
 *
 * @deprecated
 */
abstract class CustomerDecorator
{

    /**
     * @var callable
     *
     * @Serialized\Exclude()
     */
    protected $loader;

    /**
     * @var Customer
     * @Serialized\Exclude()
     */
    protected $decorated;

    /**
     * @return string
     */
    public function getEmail()
    {
        $this->ensureLoaded();

        return $this->decorated->getEmail();
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->decorated->setEmail($email);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMobilePhone()
    {
        $this->ensureLoaded();

        return $this->decorated->getMobilePhone();
    }

    /**
     * @param string $mobilePhone
     * @return $this
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->decorated->setMobilePhone($mobilePhone);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPostAddress()
    {
        $this->ensureLoaded();

        return $this->decorated->getPostAddress();
    }

    /**
     * Ensuring that customer loaded
     */
    protected function ensureLoaded()
    {
        if ($this->decorated) {
            return;
        }

        if (!$loader = $this->loader) {
            throw new \RuntimeException('Loader not passed');
        }

        $this->decorated = $loader();
    }

    /**
     * @return Customer
     */
    public function getDecorated()
    {
        $this->decorateDown();
        return $this->decorated;
    }

    /**
     * @param Customer $decorated
     * @return self
     */
    public function setDecorated($decorated)
    {
        $this->decorated = $decorated;
//        $this->id = $decorated->getId();

        $this->decorateUp();

        return $this;
    }

    /**
     * @param callable $loader
     * @return self
     */
    public function setLoader($loader)
    {
        $this->loader = $loader;

        return $this;
    }

    /**
     * Move data from "decorated" to decorator (prepare entity for saving to DB).
     *
     * @return void
     */
    public abstract function decorateUp();

    /**
     * Move data from decorator to CRM-model (prepare data for transporting to CRM)
     *
     * @return void
     */
    public abstract function decorateDown();
}