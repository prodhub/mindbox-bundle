<?php

namespace ADW\MindboxBundle\Customer;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class CustomerBirthDay
 *
 * @package ADW\MindboxBundle\Customer
 * @author Anton Prokhorov
 */
class CustomerBirthDay
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("year")
     * @Serialized\Groups({"get", "save"})
     */
    protected $year;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("month")
     * @Serialized\Groups({"get", "save"})
     */
    protected $month;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("day")
     * @Serialized\Groups({"get", "save"})
     */
    protected $day;

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     * @return CustomerBirthDay
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return string
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param string $month
     * @return CustomerBirthDay
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param string $day
     * @return CustomerBirthDay
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @param \DateTime $dateTime
     * @return $this
     */
    public function fromDateTime(\DateTime $dateTime)
    {
        $this->year = $dateTime->format('Y');
        $this->month = $dateTime->format('m');
        $this->day = $dateTime->format('d');

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function toDateTime()
    {
        if (!$this->year || !$this->month || !$this->day) return null;

        return new \DateTime(
            sprintf('%s-%s-%s', $this->year, $this->month, $this->day)
        );
    }
}