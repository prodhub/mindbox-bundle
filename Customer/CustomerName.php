<?php

namespace ADW\MindboxBundle\Customer;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class CustomerName
 *
 * @package ADW\MindboxBundle\Customer
 * @author Artur Vesker
 */
class CustomerName
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("last")
     * @Serialized\Groups({"get", "save"})
     */
    protected $last;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("first")
     * @Serialized\Groups({"get", "save"})
     */
    protected $first;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("middle")
     * @Serialized\Groups({"get", "save"})
     */
    protected $middle;

    /**
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @return string
     */
    public function getMiddle()
    {
        return $this->middle;
    }

    /**
     * @return string
     */
    public function getFull()
    {
        return implode(' ', [$this->first, $this->middle, $this->last]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFull();
    }

    /**
     * @param string $first
     * @return CustomerName
     */
    public function setFirst($first)
    {
        $this->first = $first;
        return $this;
    }

    /**
     * @param string $middle
     * @return CustomerName
     */
    public function setMiddle($middle)
    {
        $this->middle = $middle;
        return $this;
    }

    /**
     * @param string $last
     * @return CustomerName
     */
    public function setLast($last)
    {
        $this->last = $last;
        return $this;
    }
}