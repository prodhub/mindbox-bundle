<?php

namespace ADW\MindboxBundle\Customer;

use ADW\MindboxBundle\RestClient\Model\Customer;

/**
 * Interface CustomerDecoratorRepositoryInterface
 *
 * @package ADW\MindboxBundle\Customer
 */
interface CustomerDecoratorRepositoryInterface
{
    /**
     * @param int $id
     * @return Customer
     */
    public function findByMindboxId($id);

    /**
     * @return Customer
     */
    public function instance();

    /**
     * @param Customer $customer
     * @return Customer
     */
    public function save(Customer $customer);

}