<?php

namespace ADW\MindboxBundle\Customer;

use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\RestClient\ServiceDescription\CustomerCurrentServiceDescription;

/**
 * Наследники данного класса будут выполнять сразу 2 функции -
 * отдавать инфу по текущему пользователю и сохранять обновленные данные в CRM
 *
 * Class AbstractCustomerOperation
 * @package AppBundle\DirectCRM
 */
abstract class AbstractCustomerOperation extends AbstractOperationDescription
{
    const MODE_GET = 'g';
    const MODE_EDIT = 'e';

    private $mode;

    /**
     * EditProfile constructor.
     * @param string $mode
     */
    public function __construct($mode = self::MODE_GET)
    {
        $this->mode = $mode;
    }

    /**
     * @inheritdoc
     */
    public function getServiceDescription()
    {
        return new CustomerCurrentServiceDescription();
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return (self::MODE_GET == $this->mode) ? 'GET' : 'PUT';
    }

    /**
     * @inheritDoc
     */
    public function getOptions()
    {
        if (self::MODE_EDIT == $this->mode) {
            return [
                'data' => Customer::class,
            ];
        }

        return [];
    }

    /**
     * @inheritDoc
     */
    public function getRequestData(array $options)
    {
        if (self::MODE_EDIT == $this->mode) {
            return $options['data'];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getRequestDataContext()
    {
        if (self::MODE_EDIT == $this->mode) {
            return ['groups' => ['save']];
        }

        return ['groups' => ['get']];
    }
}