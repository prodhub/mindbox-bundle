<?php

namespace ADW\MindboxBundle\Customer;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class CustomerSubscription
 *
 * @package ADW\MindboxBundle\Customer
 * @author Anton Prokhorov
 */
class CustomerSubscription
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("isActiveForCurrentBrand")
     * @Serialized\Groups({"get", "save"})
     */
    protected $isActiveForCurrentBrand = 'true';

    /**
     * @return string
     */
    public function getIsActiveForCurrentBrand()
    {
        return $this->isActiveForCurrentBrand;
    }

    /**
     * @param string $isActiveForCurrentBrand
     */
    public function setIsActiveForCurrentBrand($isActiveForCurrentBrand)
    {
        $this->isActiveForCurrentBrand = $isActiveForCurrentBrand;
    }

}