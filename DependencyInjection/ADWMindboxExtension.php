<?php

namespace ADW\MindboxBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Vesax\GuzzleBundle\DependencyInjection\GuzzleDependencyInjectionTrait;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ADWMindboxExtension extends Extension
{

    use GuzzleDependencyInjectionTrait;

    const GUZZLE_ID = 'adw_mindbox.guzzle';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        //set config
        foreach ($config as $key => $value) {
            $container->setParameter('adw_mindbox.config.' . $key, $value);
        }
        $container->setParameter('adw_mindbox.config.staff.login', $config['staff']['login']);
        $container->setParameter('adw_mindbox.config.staff.password', $config['staff']['password']);

        $config['guzzle_options'] = [
            'headers' => [
                'User-Agent' => 'ADW',
            ],
        ];

        //set guzzle
        if ($config['guzzle']) {
            $container->setAlias(self::GUZZLE_ID, $config['guzzle']);
        } else {
            $stack = [];

            //configure cache
            if ($config['cache']) {
                $this->buildCacheHandler($container, $config['cache']);
                $stack[] = new Reference('adw_mindbox.cache_middleware');
                $stack[] = new Reference('adw_mindbox.cache_crutch_middleware');
            }

            $logger = $this->buildLogger($container, 'mindbox', 'mindbox', '>>> {request} <<< {response}');

            $guzzle = $this->buildClient($container, 'mindbox', null, $stack, $config['guzzle_options'], true, $logger);
            $container->setAlias(self::GUZZLE_ID, $guzzle);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter($this->getAlias() . '.config.customer_decorator_class',
            $config['customer_decorator_class']);
        $container->setParameter($this->getAlias() . '.config.customer_edit_type_class',
            $config['customer_edit_type_class']);

        if ($config['customer_decorator_repository']) {
            $container->findDefinition('adw_mindbox.security.user_provider')
                ->replaceArgument(2, new Reference($config['customer_decorator_repository']));
        }
    }

    /**
     * @param ContainerBuilder $container
     * @param $cacheProvider
     */
    protected function buildCacheHandler(ContainerBuilder $container, $cacheProvider)
    {
        $container->setDefinition('adw_mindbox.cache_storage',
            new Definition('Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage',
                [new Reference('doctrine_cache.providers.' . $cacheProvider)]));
        $container->setDefinition('adw_mindbox.cache_strategy',
            new Definition('Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy',
                [new Reference('adw_mindbox.cache_storage')]));
        $container->setDefinition('adw_mindbox.cache_middleware',
            new Definition('Kevinrob\GuzzleCache\CacheMiddleware', [new Reference('adw_mindbox.cache_strategy')]));
        $container->setDefinition('adw_mindbox.cache_crutch_middleware',
            new Definition('ADW\mindboxBundle\Middleware\CacheCrutch'));
    }

    /**
     * @param $name
     * @param $method
     * @return string
     */
    protected function generateAlias($name, $method)
    {
        return strtolower(implode('_', array_merge([$method], explode('.', $name))));
    }
}
