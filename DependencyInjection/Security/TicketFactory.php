<?php

namespace ADW\MindboxBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class TicketFactory
 *
 * @package ADW\MindboxBundle\DependencyInjection\Security
 * @author Artur Vesker
 */
class TicketFactory implements SecurityFactoryInterface
{

    /**
     * @inheritdoc
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $template = 'adw_mindbox.security.ticket_listener';
        $listenerDefinition = new DefinitionDecorator($template);
        $listenerDefinition
            ->addArgument($id)
            ->addArgument($config['name'])
        ;
        $listenerId = $template . '.' . $id;
        $container->setDefinition($listenerId, $listenerDefinition);

        $template = 'adw_mindbox.security.ticket_authentication_provider';
        $providerDefinition = new DefinitionDecorator($template);
        $providerDefinition
            ->addArgument(new Reference($userProvider))
            ->addArgument($id);
        $providerId = $template . '.' . $id;
        $container->setDefinition($providerId, $providerDefinition);

        return [$providerId, $listenerId, $defaultEntryPoint];
    }

    /**
     * @inheritdoc
     */
    public function addConfiguration(NodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode('name')->defaultValue('direct-crm-ticket')->end()
                ->scalarNode('provider')->defaultValue('mindbox_user')->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'adw_mindbox.security.ticket_authentication_provider.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('adw_mindbox.security.ticket_authentication_provider'));

        return $provider;
    }


    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'http';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'adw_mindbox_ticket';
    }

}