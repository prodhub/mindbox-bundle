<?php

namespace ADW\MindboxBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class UsernamePasswordFactory
 *
 * @author Artur Vesker
 */
class UsernamePasswordFactory extends FormLoginFactory
{

    /**
     * @inheritdoc
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $providerId = 'mindbox.security.username_password_authentication_provider';

        $definition = new DefinitionDecorator($providerId);
        $definition
            ->addArgument($id)
            ->addArgument(new Reference('mindbox_client'))
            ->addArgument(new Reference($userProviderId))
        ;

        $providerId = $providerId . '.' . $id;

        $container->setDefinition($providerId, $definition);

        return $providerId;
    }

    /**
     * @inheritdoc
     */
    protected function getListenerId()
    {
        return 'security.authentication.listener.form';
    }

    public function getPosition()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function getKey()
    {
        return 'mindbox_username_password';
    }

}