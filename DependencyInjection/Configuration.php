<?php

namespace ADW\MindboxBundle\DependencyInjection;

use ADW\MindboxBundle\Form\Type\RegistrationType;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_mindbox');

        $rootNode
            ->children()
                /*->arrayNode('services')
                    ->useAttributeAsKey('key')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('path')->defaultNull()->end()
                            ->scalarNode('method')->isRequired()->end()
                            ->scalarNode('model')->defaultNull()->end()
                        ->end()
                    ->end()
                ->end()*/
                /*->arrayNode('operations')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')->isRequired()->end()
                            ->scalarNode('alias')->isRequired()->end()
                            ->scalarNode('method')->isRequired()->end()
                            ->scalarNode('service')->isRequired()->end()
                            ->scalarNode('model')->defaultNull()->end()
                            ->arrayNode('options')
                                ->prototype('scalar')->end()
                            ->end()
                            ->booleanNode('staff')->defaultFalse()->end()
                        ->end()
                    ->end()
                ->end()*/

                ->arrayNode('customer_get_operations')
                    ->prototype('scalar')
                    ->end()
                ->end()

                ->arrayNode('customer_edit_operations')
                    ->prototype('scalar')
                    ->end()
                ->end()

                ->scalarNode('social_network_registration_operation')->defaultNull()->end()

                ->scalarNode('social_account_parse_strategy_factory')->defaultNull()->end()

                ->scalarNode('host')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('brand')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('channel')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('key')->isRequired()->cannotBeEmpty()->end()

                ->arrayNode('staff')
                    ->children()
                        ->scalarNode('login')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('password')->isRequired()->cannotBeEmpty()->end()
                    ->end()
                ->end()

                ->scalarNode('guzzle')->defaultNull()->end()
                ->scalarNode('cache')->defaultNull()->end()
                ->scalarNode('customer_decorator_class')->isRequired()->end()
                ->scalarNode('customer_edit_type_class')->isRequired()->end()
//                ->scalarNode('current_user_operation')->isRequired()->end()
//                ->scalarNode('edit_user_operarion')->isRequired()->end()
                ->scalarNode('customer_decorator_repository')->defaultNull()->end()
                ->scalarNode('user_repo')->defaultNull()->end()

                ->scalarNode('reg_form_type')->defaultValue(RegistrationType::class)->cannotBeEmpty()->end()
                ->scalarNode('reg_operation')->defaultNull()->cannotBeEmpty()->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
