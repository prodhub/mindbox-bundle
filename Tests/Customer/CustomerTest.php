<?php

namespace ADW\MindboxBundle\Tests\Contact;

use JMS\Serializer\SerializerBuilder;

/**
 * Class CustomerTest
 *
 * @package ADW\MindboxBundle\Tests\Customer
 * @author Artur Vesker
 */
class CustomerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test default user deserialization
     */
    public function testDeserialization()
    {
        $serializer = SerializerBuilder::create()->build();

        $xml =  <<<EOD
<customer id="1000632">
 <idHistory />
 <version>UZPj+Jwi0wg=</version>
 <name last="Орехов" first="Иван" middle="Владимирович" />
 <mobilePhone>+79168675634</mobilePhone>
 <postAddress postIndex="123456" house="" estate="" building="" unit="" flat="">
  <region id="1000629" name="Нижегородская">
    <type id="1000628" fullName="область" shortName="обл." optimalName="обл." />
  </region>
  <district id="" name="">
    <type id="" fullName="" shortName="" optimalName="" />
  </district>
  <settlement id="1000626" name="Нижний Новгород">
    <type id="1000630" fullName="город" shortName="г." optimalName="г." />
  </settlement>
  <street id="" name="">
    <type id="" fullName="" shortName="" optimalName="" />
  </street>
  <comments></comments>
 </postAddress>
 <subscription isActiveForCurrentBrand="true" />
 <externalIdentities>
  <externalIdentity provider="Mac" value="123654" />
 </externalIdentities>
 <clinicId>123123</clinicId>
</customer>
EOD;

        /**
         * @var \ADW\MindboxBundle\RestClient\Model\Customer $customer
         */
        $customer = $serializer->deserialize($xml, 'ADW\MindboxBundle\RestClient\Model\Customer', 'xml');

        $this->assertInstanceOf('ADW\MindboxBundle\RestClient\Model\Customer', $customer);
        $this->assertEquals(1000632, $customer->getId());
        $this->assertEquals("+79168675634", $customer->getMobilePhone());
        $this->assertEquals(123456, $customer->getPostAddress()->getPostIndex());
    }

}