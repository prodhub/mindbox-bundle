<?php

namespace ADW\MindboxBundle\Tests\RestClient;

use JMS\Serializer\SerializerBuilder;

/**
 * Class SuccessResponseTest
 *
 * @author Artur Vesker
 */
class SuccessResponseTest extends \PHPUnit_Framework_TestCase
{

    public function testDeserialization()
    {
        $serializer = SerializerBuilder::create()->build();

        $xml = <<<EOD
<success>
  <messages>
    <message>Персональная информация успешно сохранена.</message>
  </messages>
  <id>1795646</id>
  <idHistory />
  <sessionKey>635768459860163920,6314CB6315C7A9D42272C34E04AEBEA030CC2C1F1545E908D4E402430CB9B29349E1E06273BAB7A6C1BE2A01ACFBD52EE8077F98732C4D55514F8C4B58C1C5DA</sessionKey>
</success>
EOD;

        $response = $serializer->deserialize($xml, 'ADW\MindboxBundle\RestClient\SuccessResponse', 'xml');

        $messages = $response->getMessages();

        $this->assertCount(1, $messages);
        $this->assertEquals('Персональная информация успешно сохранена.', $messages[0]);
    }

}
