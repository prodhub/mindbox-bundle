<?php

namespace ADW\MindboxBundle\Validation;

/**
 * Class ConstraintViolationListFactoryTest
 *
 * @author Artur Vesker
 */
class ConstraintViolationListFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testFormXmlWithOneError()
    {
        $factory = new ConstraintViolationListFactory();

        $xml = <<<EOT
<?xml version="1.0" encoding="utf-8"?>
<validationErrors>
  <validationError>
    <message>Ссылка неверная</message>
    <htmlMessage><![CDATA[Ссылка неверная]]></htmlMessage>
  </validationError>
</validationErrors>
EOT;

        $list = $factory->fromXml($xml);

        $this->assertCount(1, $list);
        $this->assertEquals('Ссылка неверная', $list[0]->getMessage());
    }

    public function testFormXmlWithFewErrors()
    {
        $factory = new ConstraintViolationListFactory();

        $xml = <<<EOT
<?xml version="1.0" encoding="utf-8"?>

<validationErrors>
        <validationError>
                <message>Неверная фамилия</message>
                <htmlMessage>Неверная фамилия</htmlMessage>
                <location>/customer/name/@last</location>
        </validationError>
        <validationError>
                <message>Заполнение адреса обязательно</message>
                <htmlMessage>Заполнение адреса обязательно</htmlMessage>
        </validationError>
        <validationError>
                <message>Не указан код</message>
                <htmlMessage>Не указан код</htmlMessage>
                <location>code</location>
        </validationError>
</validationErrors>
EOT;

        $list = $factory->fromXml($xml, function($key) {
            return [
                '/customer/name/@last' => 'last_name',
                'code' => 'code'
            ][$key];
        });

        $this->assertCount(3, $list);
        $this->assertEquals('Неверная фамилия', $list[0]->getMessage());
        $this->assertEquals('last_name', $list[0]->getPropertyPath());
        $this->assertEquals('Заполнение адреса обязательно', $list[1]->getMessage());
        $this->assertEquals(null, $list[1]->getPropertyPath());
    }

}