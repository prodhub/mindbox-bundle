<?php

namespace ADW\MindboxBundle\Tests\Contact;

use JMS\Serializer\SerializerBuilder;

/**
 * Class PostAddressTest
 *
 * @package ADW\MindboxBundle\Tests\Contact
 * @author Artur Vesker
 */
class PostAddressTest extends \PHPUnit_Framework_TestCase
{


    public function testDeserialization()
    {
        $serializer = SerializerBuilder::create()->build();

        $xml =  <<<EOD
<postAddress postIndex="123456" house="2" estate="" building="" unit="" flat="">
  <region id="1000629" name="Нижегородская">
    <type id="1000628" fullName="область" shortName="обл." optimalName="обл." />
  </region>
  <district id="" name="">
    <type id="" fullName="" shortName="" optimalName="" />
  </district>
  <settlement id="1000626" name="Нижний Новгород">
    <type id="1000630" fullName="город" shortName="г." optimalName="г." />
  </settlement>
  <street id="" name="">
    <type id="" fullName="" shortName="" optimalName="" />
  </street>
  <comments></comments>
 </postAddress>
EOD;

        /**
         * @var \ADW\MindboxBundle\Contact\PostAddress $postAddress
         */
        $postAddress = $serializer->deserialize($xml, 'ADW\MindboxBundle\Contact\PostAddress', 'xml');


        $this->assertInstanceOf('ADW\MindboxBundle\Contact\PostAddress', $postAddress);
        $this->assertEquals(123456, $postAddress->getPostIndex());
        $this->assertEquals("2", $postAddress->getHouse());
        $this->assertEquals(1000629, $postAddress->getRegion()->getId());
        $this->assertEquals("Нижегородская", $postAddress->getRegion()->getName());

    }

}