<?php

namespace ADW\MindboxBundle\Repository;

use ADW\MindboxBundle\Customer\AbstractCustomerOperation;
use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\Exception\OperationException;
use GuzzleHttp\Exception\ClientException;
use Vesax\RestClientBundle\Client\Client;

/**
 * Class CustomerRepository
 *
 * @author Anton Prokhorov
 * @author Sergey Cherkesov
 */
class CustomerRepository
{
    /** @var Client */
    protected $client;

    /** @var array */
    protected $editCustomerOperations;

    /**
     * CustomerRepository constructor.
     * @param Client $client
     * @param array $editCustomerOperations
     */
    public function __construct(Client $client, array $editCustomerOperations)
    {
        $this->client = $client;
        $this->editCustomerOperations = $editCustomerOperations;
    }

    /**
     * Create update existing.
     *
     * @param Customer $customer
     * @return $this
     * @throws \Exception
     */
    public function save(Customer $customer)
    {
        $customer->decorateDown();

        if (!$id = $customer->getId()) {
            throw new \Exception('User must have ID');
        } else {
            foreach ($this->editCustomerOperations as $operationClassName) {
                try {
                    /** @var AbstractCustomerOperation $operation */
                    $operation = new $operationClassName(AbstractCustomerOperation::MODE_EDIT);
                    $this->client->request($operation, ['data' => $customer]);

                    return $this;
                } catch (ClientException $e) {
                    // TODO: Нужно как-то реагировать на ошибки Http-запроса
                } catch (OperationException $e) {
                    // В конфигурации описаны все операции получения профиля
                    // и понятное дело что некоторые из них будут недоступны
                    // для определенных пользователей.
                    // Поэтому мы игнорирует недоступные операции
                }
            }

            throw new \LogicException(
                sprintf('All save customer operation is invalid!')
            );
        }
    }
}
