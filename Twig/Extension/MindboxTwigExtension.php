<?php

namespace ADW\MindboxBundle\Twig\Extension;

use ADW\MindboxBundle\Operation\OperationsRepository;

/**
 * Class MindboxTwigExtension
 *
 * @package ADW\MindboxBundle\Twig
 * @author Artur Vesker
 */
class MindboxTwigExtension extends \Twig_Extension
{

    /**
     * @var OperationsRepository
     */
    protected $operationRepository;

    /**
     * @param OperationsRepository $operationRepository
     */
    public function __construct(OperationsRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('mindbox_allowed', function($value) {
                return $this->operationRepository->findByName($value)->isAllowed();
            })
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'mindbox';
    }

}