<?php

namespace ADW\MindboxBundle\Security\User;

use ADW\MindboxBundle\Security\Token\MindboxToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserRepository
 *
 * @author Artur Vesker
 */
interface UserRepositoryInterface
{
    /**
     * @param MindboxToken $token
     * @return UserInterface
     */
    public function get(MindboxToken $token);

    /**
     * @param $user
     * @return mixed
     */
    public function save($user);
}