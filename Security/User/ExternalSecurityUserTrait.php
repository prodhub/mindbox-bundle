<?php

namespace ADW\MindboxBundle\Security\User;

trait ExternalSecurityUserTrait
{
    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        if ($this->getEmail()) {
            return $this->getEmail();
        }

        if ($this->getMobilePhone()) {
            return $this->getMobilePhone();
        }

        if ($this->getId()) {
            return $this->getId();
        }

        return 'undefined';
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
        return;
    }
}