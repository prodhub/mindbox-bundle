<?php

namespace ADW\MindboxBundle\Security\User;

use ADW\MindboxBundle\Customer\AbstractCustomerOperation;
use ADW\MindboxBundle\Customer\CustomerDecoratorRepositoryInterface;
use ADW\MindboxBundle\Exception\OperationException;
use ADW\MindboxBundle\Operation\OperationsRepository;
use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\RestClient\OperationDescription\AbstractOperationDescription;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Vesax\RestClientBundle\Client\Client;

/**
 * Class MindboxUserProvider
 *
 * @author Artur Vesker
 * @author Cherkesov Sergey
 */
class MindboxUserProvider implements UserProviderInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var Client */
    protected $client;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var CustomerDecoratorRepositoryInterface */
    protected $customerDecoratorRepository;

    /** @var string */
    protected $customerClass;

    /** @var array */
    protected $getCustomerOperations;

    /** @var OperationsRepository */
    protected $operationsRepo;


    /**
     * @param Client $client
     * @param TokenStorageInterface $tokenStorage
     * @param CustomerDecoratorRepositoryInterface|null $customerDecoratorRepository
     * @param string $customerClass
     * @param array $getCustomerOperations
     * @param OperationsRepository $operationsRepo
     */
    public function __construct(
        Client $client,
        TokenStorageInterface $tokenStorage,
        CustomerDecoratorRepositoryInterface $customerDecoratorRepository = null,
        $customerClass,
        array $getCustomerOperations,
        $operationsRepo
    ) {
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
        $this->customerDecoratorRepository = $customerDecoratorRepository;
        $this->customerClass = $customerClass;
        $this->getCustomerOperations = $getCustomerOperations;
        $this->operationsRepo = $operationsRepo;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByUsername($username)
    {
        // При работает с CRM можно получить только текущего авторизованного пользователя,
        // поэтому чтоб не подавать разработчику ложных надежд
        // этот метод "приглушен" исключением.
        throw new \RuntimeException('Are you serious? You can use only current user');
    }

    /**
     * @inheritdoc
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        if (!$user instanceof MindboxToken) {
            return $user;
        }

        return $this->getCurrentUser($user);
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class)
    {
        return
            $class === $this->customerClass
            || $class === MindboxToken::class
            || is_subclass_of(new $class(), Customer::class);
    }

    /**
     * @inheritdoc
     */
    public function getCurrentUser(MindboxToken $token)
    {
//        dump($token);
        $this->tokenStorage->setToken($token);

        $customer = null;
        foreach ($this->getCustomerOperations as $operationClassName) {
            try {
                $operation = new $operationClassName();
                $this->checkIsValidGetUserOperation($operation);
                $customer = $this->client->request($operation);
                break;
            } catch (ClientException $e) {
                // TODO: Нужно как-то реагировать на ошибки Http-запроса
//                dump($e);
//                dump($e->getResponse()->getBody()->getContents());
//                die;
            } catch (OperationException $e) {
                // В конфигурации описаны все операции получения профиля
                // и понятное дело что некоторые из них будут недоступны
                // для определенных пользователей.
                // Поэтому мы игнорирует недоступные операции
//                dump($e);
//                die;
            }
        }

        if (null == $customer) {
            $this->tokenStorage->setToken(null);
            throw new AuthenticationException('Invalid credentials');
        }

        if (!$this->customerDecoratorRepository) {
            return $customer;
        }

        if ($local = $this->customerDecoratorRepository->findByMindboxId($customer->getId())) {
            $local->decorate($customer);

            return $local;
        }

        $customerDecorator = $this->customerDecoratorRepository->instance();
        $customerDecorator->decorate($customer);

        return $this->customerDecoratorRepository->save($customerDecorator);
    }

    /**
     * @param mixed|AbstractOperationDescription $operation
     */
    private function checkIsValidGetUserOperation($operation)
    {
        if (!$operation instanceof AbstractCustomerOperation) {
            throw new \LogicException(
                sprintf(
                    'Operation class "%s" must extends "%s"',
                    get_class($operation),
                    AbstractCustomerOperation::class
                )
            );
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}