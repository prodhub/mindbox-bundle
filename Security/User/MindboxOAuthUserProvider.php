<?php

namespace ADW\MindboxBundle\Security\User;

use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\RestClient\Model\ExternalIdentity;
use ADW\MindboxBundle\RestClient\OperationDescription\DirectCrm\ExternalLogOn;
use ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current\LogonServiceDescription;
use ADW\MindboxBundle\Security\Credentials\ExternalCredentials;
use ADW\MindboxBundle\Security\Model\SocialAccountInfo;
use ADW\MindboxBundle\Security\Service\SocialAccountParser;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Vesax\RestClientBundle\Client\Client;

class MindboxOAuthUserProvider extends OAuthUserProvider implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var Client */
    protected $client;

    /**
     * @var SocialAccountParser
     */
    private $socialAccountParser;

    /**
     * MindboxOAuthUserProvider constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param Client $client
     * @param SocialAccountParser $socialAccountParser
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        Client $client,
        SocialAccountParser $socialAccountParser
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->client = $client;
        $this->socialAccountParser = $socialAccountParser;
    }

    /**
     * @param SocialAccountInfo $socialAccountInfo
     * @return Customer|mixed|null
     * @throws \Exception
     */
    public function loadUserByUsername($socialAccountInfo)
    {
        $uid = $socialAccountInfo->getUid();
        $provider = $socialAccountInfo->getProvider();

        // Нужно осуществить привязку профиля из соц сети
        // Рассматриваем 3 случая:
        //   1. Авторизован
        //   2. Неавторизован и уже имеет привязку в CRM
        //   3. Неавторизован и еще не имеет привязки в CRM

        $customer = $this->getCustomer();
        if (null != $customer) {
            // Пользователь авторизован
            // К текущему профилю привязываем профили из соц сетей
            $ei = new ExternalIdentity($provider, $uid);
            $customer->setExternalIdentity($ei);

            $this->getCustomerRepo()->save($customer);

            $customer = $this->socialAccountParser->decorate($customer, $socialAccountInfo);

            // TODO: ловить событие сохранения профиля пользователя и писать копию в локальную БД на событие
            $em = $this->getDoctrine()->getManager();
            $em->merge($customer);
            $em->flush();
        } else {
            try {
                // Пробуем авторизовать пользователя в соц сети
                $credentials = new ExternalCredentials($provider, $uid);

                /** @var MindboxToken $token */
                $token = $this->getCRM()->request(
                    new ExternalLogOn(),
                    [
                        'credentials' => $credentials,
                    ]
                );

                $customer = $this->getUserProvider()->getCurrentUser($token);
            } catch (\Exception $exception) {
                $socRegClassName = $this->container
                    ->getParameter('adw_mindbox.config.social_network_registration_operation');

                // Mindbox на заказ может сделать операции авторизации из соц сетей.
                // Если у вас нет такой операции, будем выбрасывать исключение, так как нет никакой возможности добавить пользователя в БД CRM
                // не прибегая к заполнению формы регистрации вручную
                if (!$socRegClassName) {
                    throw  $exception;
                }

                $token = $this->getCRM()->request(
                    new $socRegClassName(),
                    [
                        'identity' => new ExternalIdentity($provider, $uid),
                    ]
                );

                $customer = $this->getUserProvider()->getCurrentUser($token);
            }

            $customer = $this->socialAccountParser->decorate($customer, $socialAccountInfo);

            $em = $this->getDoctrine()->getManager();
            $em->merge($customer);
            $em->flush();

            $token->setUser($customer);
            $this->container->get('security.token_storage')->setToken($token);

            $response = new RedirectResponse($this->getRouter()->generate('homepage'));

            //  Обрываем аутентификацию HWI во избежание установки неверного токена в TokenStorage
            return $response->send();
        }

        return $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $provider = $response->getResourceOwner()->getName();
        switch ($provider) {
            case ExternalIdentity::PROVIDER_VK:
                $uid = $response->getResponse()['response'][0]['uid'];
                break;
            case ExternalIdentity::PROVIDER_FB:
                $uid = $response->getResponse()['id'];
                break;
            case ExternalIdentity::PROVIDER_OK:
                $uid = $response->getResponse()['uid'];
                break;
            // TODO: write get-id logic for OK, IG
            default:
                throw new \Exception('Please write get-id-logic for ' . $provider . ' provider!');
        }

        $socialAccountInfo = $this->socialAccountParser->parseResponse($uid, $response);

        return $this->loadUserByUsername($socialAccountInfo);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class)
    {
        return $class === MindboxToken::class;
    }

    /**
     * @return null|Customer
     */
    public function getCustomer()
    {
        $token = $this->tokenStorage->getToken();
        if (null == $token || !$token instanceof MindboxToken) {
            return null;
        }

        $customer = $token->getUser();
        if (null == $customer || !$customer instanceof UserInterface) {
            return null;
        }

        return $customer;
    }

    /**
     * @return \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    /**
     * @return \ADW\MindboxBundle\Security\User\MindboxUserProvider|object
     */
    private function getUserProvider()
    {
        return $this->container->get('adw_mindbox.security.user_provider');
    }

    /**
     * @return \ADW\MindboxBundle\Repository\CustomerRepository|object
     */
    private function getCustomerRepo()
    {
        return $this->container->get('adw_mindbox.customer_repository');
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return object|\Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private function getRouter()
    {
        return $this->container->get('router');
    }

    /**
     * @return \ADW\MindboxBundle\Client\Client|object
     */
    private function getCRM()
    {
        return $this->container->get('adw_mindbox_client');
    }
}
