<?php

namespace ADW\MindboxBundle\Security\ParseStrategy;

interface StrategyFactoryInterface
{
    /**
     * @return StrategyInterface
     */
    public function build();
}
