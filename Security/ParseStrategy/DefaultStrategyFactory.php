<?php

namespace ADW\MindboxBundle\Security\ParseStrategy;

use Symfony\Component\DependencyInjection\ContainerInterface;

class DefaultStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @return DefaultStrategy
     */
    public function build()
    {
        return new DefaultStrategy();
    }
}
