<?php

namespace ADW\MindboxBundle\Security\ParseStrategy;

use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\Security\Model\SocialAccountInfoInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;

interface StrategyInterface
{
    public function parseResponse($uid, UserResponseInterface $response);

    public function decorate(Customer $customer, SocialAccountInfoInterface $socialAccountInfo);
}
