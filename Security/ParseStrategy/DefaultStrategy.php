<?php

namespace ADW\MindboxBundle\Security\ParseStrategy;

use ADW\MindboxBundle\RestClient\Model\Customer;
use ADW\MindboxBundle\Security\Model\SocialAccountInfo;
use ADW\MindboxBundle\RestClient\Model\ExternalIdentity;
use ADW\MindboxBundle\Security\Model\SocialAccountInfoInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;

class DefaultStrategy implements StrategyInterface
{
    public function parseResponse($uid, UserResponseInterface $response)
    {
        $provider = $response->getResourceOwner()->getName();
        switch ($provider) {
            case ExternalIdentity::PROVIDER_VK:
                $uid = $response->getResponse()['response'][0]['uid'];
                break;
            case ExternalIdentity::PROVIDER_FB:
                $uid = $response->getResponse()['id'];
                break;
            // TODO: write get-id logic for OK, IG
            default:
                throw new \Exception('Please write get-id-logic for ' . $provider . ' provider!');
        }

        $socialAccountInfo = new SocialAccountInfo();
        $socialAccountInfo->setUid($uid);
        $socialAccountInfo->setProvider($provider);

        return $socialAccountInfo;
    }

    public function decorate(Customer $customer, SocialAccountInfoInterface $socialAccountInfo)
    {
        return $customer;
    }
}
