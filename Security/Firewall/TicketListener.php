<?php

namespace ADW\MindboxBundle\Security\Firewall;

use ADW\MindboxBundle\Security\Token\TicketToken;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Class TicketListener
 *
 * @package ADW\MindboxBundle\Security\Firewall
 * @author Artur Vesker
 */
class TicketListener implements ListenerInterface
{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @var string
     */
    protected $queryFieldName = 'direct-crm-ticket';

    /**
     * @var string
     */
    protected $key;

    /**
     * TicketListener constructor.
     *
     * @param AuthenticationManagerInterface $authenticationManager
     * @param $key
     * @param string $queryFieldName
     */
    public function __construct(AuthenticationManagerInterface $authenticationManager, TokenStorageInterface $tokenStorage, $key, $queryFieldName = null)
    {
        $this->authenticationManager = $authenticationManager;
        $this->tokenStorage = $tokenStorage;
        $this->queryFieldName = $queryFieldName;
        $this->key = $key;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function handle(GetResponseEvent $event)
    {
        if ($this->tokenStorage->getToken()) {
            return;
        }

        $request = $event->getRequest();

        if (!$ticket = $request->query->get($this->queryFieldName)) {
            return;
        }

        try {
            $this->tokenStorage->setToken($this->authenticationManager->authenticate(new TicketToken($ticket, $this->key)));
        } catch (AuthenticationException $e) {
            //do nothing
        }
    }

}