<?php

namespace ADW\MindboxBundle\Security\Service;

use ADW\MindboxBundle\Security\ParseStrategy\StrategyInterface;
use ADW\MindboxBundle\Security\ParseStrategy\DefaultStrategyFactory;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SocialAccountParser
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var StrategyInterface
     */
    private $strategy;

    /**
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;

        if ($strategyFactoryClass = $container->getParameter('adw_mindbox.config.social_account_parse_strategy_factory')) {
            $factory = new $strategyFactoryClass($container);
        } else {
            $factory = new DefaultStrategyFactory($container);
        }

        $this->strategy = $factory->build();
    }

    public function parseResponse($uid, $response)
    {
        return $this->strategy->parseResponse($uid, $response);
    }

    public function decorate($customer, $socialAccountInfo)
    {
        return $this->strategy->decorate($customer, $socialAccountInfo);
    }
}
