<?php

namespace ADW\MindboxBundle\Security\Token;

/**
 * Class TicketToken
 *
 * @package ADW\MindboxBundle\Security\Token
 * @author Artur Vesker
 */
class TicketToken extends MindboxToken
{

    /**
     * @var string
     */
    protected $ticket;

    /**
     * TicketToken constructor.
     *
     * @param null|string $ticket
     * @param null|string $key
     * @param null $customerId
     * @param null $sessionKey
     */
    public function __construct($ticket, $key, $customerId = null, $sessionKey = null)
    {
        $this->ticket = $ticket;
        parent::__construct($key, $customerId, $sessionKey);
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

}