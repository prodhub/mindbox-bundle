<?php

namespace ADW\MindboxBundle\Security\Token;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class MindboxToken
 *
 * @author Artur Vesker
 * @Serialized\ExclusionPolicy("ALL")
 * @Serialized\XmlRoot("success")
 */
class MindboxToken implements TokenInterface, UserInterface
{
    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var bool
     */
    protected $authenticated = false;

    /**
     * @var int
     *
     * @Serialized\Expose()
     * @Serialized\Type("integer")
     * @Serialized\SerializedName("id")
     */
    protected $customerId;

    /**
     * @var string
     *
     * @Serialized\Expose()
     * @Serialized\Type("string")
     * @Serialized\SerializedName("sessionKey")
     */
    protected $sessionKey;

    /**
     * @var string
     *
     * @Serialized\Expose()
     * @Serialized\Type("string")
     * @Serialized\SerializedName("permanentAuthenticationKey")
     */
    protected $permanentAuthenticationKey;

    /**
     * @var string
     */
    protected $key;

    /**
     * @param $key
     * @param string|null $customerId
     * @param string|null $sessionKey
     */
    public function __construct($key, $customerId = null, $sessionKey = null)
    {
        $this->customerId = $customerId;
        $this->sessionKey = $sessionKey;
        $this->key = $key;
        $this->refreshAuthenticationStatus();
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return MindboxToken
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser($user)
    {
        if (!($user instanceof UserInterface || (is_object($user) && method_exists($user,
                    '__toString')) || is_string($user))
        ) {
            throw new \InvalidArgumentException(
                '$user must be an instanceof UserInterface, an object implementing a __toString method, or a primitive string.'
                . gettype($user) . ' given.'
            );
        }

        if ($user instanceof UserInterface) {
            $this->setRoles($user->getRoles());
        }

        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    private function setRoles($roles)
    {
        $this->roles = [];

        foreach ($roles as $role) {
            if (is_string($role)) {
                $role = new Role($role);
            } elseif (!$role instanceof RoleInterface) {
                throw new \InvalidArgumentException(sprintf('$roles must be an array of strings, or RoleInterface instances, but got %s.',
                    gettype($role)));
            }

            $this->roles[] = $role;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        if ($this->user instanceof UserInterface) {
            return $this->user->getUsername();
        }

        return (string)$this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function isAuthenticated()
    {
        return ($this->user && $this->sessionKey && $this->customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
        $this->authenticated = (bool)$authenticated;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        if ($this->getUser() instanceof UserInterface) {
            $this->getUser()->eraseCredentials();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(
            [
                $this->attributes,
                $this->key,
                $this->customerId,
                $this->sessionKey,
                $this->permanentAuthenticationKey,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list(
            $this->attributes,
            $this->key,
            $this->customerId,
            $this->sessionKey,
            $this->permanentAuthenticationKey,
            ) = unserialize($serialized);
        $this->user = $this;
        $this->refreshAuthenticationStatus();
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute($name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new \InvalidArgumentException(sprintf('This token has no "%s" attribute.', $name));
        }

        return $this->attributes[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        $class = get_class($this);
        $class = substr($class, strrpos($class, '\\') + 1);

        $roles = array();
        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }

        return sprintf('%s(user="%s", authenticated=%s, roles="%s")', $class, $this->getUsername(),
            json_encode($this->authenticated), implode(', ', $roles));
    }

    /**
     * @Serialized\PostDeserialize()
     */
    public function refreshAuthenticationStatus()
    {
        $this->authenticated = $this->sessionKey && $this->customerId;
    }

    /**
     * @internal
     */
    public function getPassword()
    {
        throw new \RuntimeException('Use user');
    }

    /**
     * @internal
     */
    public function getSalt()
    {
        throw new \RuntimeException('Use user');
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;
    }

    /**
     * @return mixed
     */
    public function getPermanentAuthenticationKey()
    {
        return $this->permanentAuthenticationKey;
    }
}