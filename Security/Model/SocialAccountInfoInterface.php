<?php

namespace ADW\MindboxBundle\Security\Model;

interface SocialAccountInfoInterface
{
    /**
     * @return string
     */
    public function getProvider();

    /**
     * @param string $provider
     * @return $this
     */
    public function setProvider($provider);

    /**
     * @return int
     */
    public function getUid();

    /**
     * @param int $uid
     * @return $this
     */
    public function setUid($uid);
}
