<?php

namespace ADW\MindboxBundle\Security\Provider;

use ADW\MindboxBundle\RestClient\ServiceDescription\Customers\Current\LogonServiceDescription;
use ADW\MindboxBundle\Security\Credentials\UsernamePasswordCredentials;
use ADW\MindboxBundle\Security\User\MindboxUserProvider;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Vesax\RestClientBundle\Client\Client;

/**
 * Class MindboxUsernamePasswordAuthenticationProvider
 *
 * @author Artur Vesker
 */
class MindboxUsernamePasswordAuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var UserCheckerInterface
     */
    protected $userChecker;

    /**
     * @var string
     */
    protected $providerKey;

    /**
     * @var MindboxUserProvider
     */
    protected $userProvider;

    /**
     * Constructor.
     *
     * @param UserCheckerInterface $userChecker An UserCheckerInterface interface
     * @param string $providerKey A provider key
     * @param Client $client
     * @param MindboxUserProvider $userProvider
     */
    public function __construct(UserCheckerInterface $userChecker, $providerKey, Client $client, MindboxUserProvider $userProvider)
    {
        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }

        $this->userChecker = $userChecker;
        $this->providerKey = $providerKey;
        $this->client = $client;
        $this->userProvider = $userProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(TokenInterface $token)
    {
//        dump($token);
//        die;

        $token = $this->client->request(new LogonServiceDescription(), [
            'credentials' => new UsernamePasswordCredentials($token->getUsername(), $token->getCredentials())
        ]);

        $token->setUser($this->userProvider->getCurrentUser($token));

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
//        dump($this, $token);
//        die();
        return $token instanceof UsernamePasswordToken && $this->providerKey === $token->getProviderKey();
    }

}