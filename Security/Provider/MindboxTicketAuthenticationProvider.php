<?php

namespace ADW\MindboxBundle\Security\Provider;

use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use ADW\MindboxBundle\Security\Token\TicketToken;
use ADW\MindboxBundle\Security\User\MindboxUserProvider;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Vesax\RestClientBundle\Client\Client;
use Vesax\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class MindboxTicketAuthenticationProvider
 *
 * @package ADW\MindboxBundle\Security\Provider
 * @author Artur Vesker
 */
class MindboxTicketAuthenticationProvider implements AuthenticationProviderInterface
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var MindboxUserProvider
     */
    protected $userProvider;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var MethodDescriptionInterface
     */
    protected $ticketAuthenticationServiceDescription;

    /**
     * MindboxTicketAuthenticationProvider constructor.
     *
     * @param Client $client
     * @param MindboxUserProvider $userProvider
     * @param MethodDescriptionInterface $ticketAuthenticationServiceDescription
     * @param string $key
     */
    public function __construct(Client $client, MethodDescriptionInterface $ticketAuthenticationServiceDescription, MindboxUserProvider $userProvider, $key)
    {
        $this->client = $client;
        $this->userProvider = $userProvider;
        $this->key = $key;
        $this->ticketAuthenticationServiceDescription = $ticketAuthenticationServiceDescription;
    }

    /**
     * @inheritdoc
     */
    public function authenticate(TokenInterface $token)
    {
        if (!$token instanceof TicketToken) {
            throw new \InvalidArgumentException('todo');
        }

        try {
            $token = $this->client->request($this->ticketAuthenticationServiceDescription, ['ticket' => $token->getTicket()]);
        } catch (ValidationViolationException $e) {
            throw new AuthenticationException('Invalid ticket');
        }

        /**
         * @var MindboxToken $token
         */
        $token->setUser($this->userProvider->getCurrentUser($token));

        return $token;
    }

    /**
     * @inheritdoc
     */
    public function supports(TokenInterface $token)
    {
        return ($token instanceof TicketToken) && ($token->getKey() === $this->key);
    }

}