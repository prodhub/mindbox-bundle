<?php

namespace ADW\MindboxBundle\Security\Credentials;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class UsernamePasswordCredentials
 *
 * @Serialized\XmlRoot("credentials")
 *
 * @author Artur Vesker
 */
class UsernamePasswordCredentials extends AbstractMindboxCredentials
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $credential;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $password;

    /**
     * UsernamePasswordCredentials constructor.
     * @param string $credential
     * @param string $password
     * @param bool $permanent
     */
    public function __construct($credential, $password, $permanent = true)
    {
        $this->credential = $credential;
        $this->password = $password;
        parent::__construct($permanent);
    }

    /**
     * @return string
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function getOperation()
    {
        return 'DirectCrm.LogOn';
    }

}