<?php

namespace ADW\MindboxBundle\Security\Credentials;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ExternalCredentials
 *
 * @Serialized\XmlRoot("credentials")
 *
 * @package ADW\MindboxBundle\Security\Credentials
 * @author Artur Vesker
 */
class ExternalCredentials extends AbstractMindboxCredentials
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $provider;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $identity;

    /**
     * ExternalCredentials constructor.
     *
     * @param bool $provider
     * @param $identity
     * @param bool $permanent
     */
    public function __construct($provider, $identity, $permanent = true)
    {
        $this->provider = $provider;
        $this->identity = $identity;
        parent::__construct($permanent);
    }

    /**
     * @inheritdoc
     */
    public function getOperation()
    {
        return 'DirectCrm.ExternalLogOn';
    }

}