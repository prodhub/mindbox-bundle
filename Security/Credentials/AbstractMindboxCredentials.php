<?php

namespace ADW\MindboxBundle\Security\Credentials;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class AbstractMindboxCredentials
 *
 * @author Artur Vesker
 */
abstract class AbstractMindboxCredentials
{

    const MODE_SESSION = 'session';
    const MODE_PERMANENT = 'permanent';

    /**
     * @var string
     *
     * @Serialized\SerializedName("mode")
     * @Serialized\XmlAttribute()
     */
    protected $mode;

    /**
     * AbstractMindboxCredentials constructor.
     *
     * @param bool $permanent
     */
    public function __construct($permanent = true)
    {
        $this->mode = $permanent ? self::MODE_PERMANENT : self::MODE_SESSION;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @return string
     */
    abstract public function getOperation();

}