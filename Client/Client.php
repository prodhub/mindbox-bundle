<?php

namespace ADW\MindboxBundle\Client;

use ADW\MindboxBundle\RestClient\ServiceDescription\CustomersCurrentLogonViaCookieServiceDescription;
use ADW\MindboxBundle\Security\Token\MindboxToken;
use ADW\MindboxBundle\Security\User\MindboxUserProvider;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Vesax\RestClientBundle\Client\Client as BaseClient;
use Vesax\RestClientBundle\Description\MethodDescriptionInterface;

class Client extends BaseClient implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    protected $container;

    public function request(
        MethodDescriptionInterface $methodDescription,
        array $options = [],
        $repeatRequest = false
    ) {
        try {
            return parent::request($methodDescription, $options);
        } catch (ClientException $e) {
            $token = $this->getTokenStorage()->getToken();
            if (false == $repeatRequest
                && !$methodDescription instanceof CustomersCurrentLogonViaCookieServiceDescription
                && Response::HTTP_UNAUTHORIZED == $e->getResponse()->getStatusCode()
                && $token instanceof MindboxToken
                && null != $token->getPermanentAuthenticationKey()
            ) {
                /** @var MindboxToken $token */
                $token = $this->request(new CustomersCurrentLogonViaCookieServiceDescription(), [
                    'customerId' => (string)$token->getCustomerId(),
                    'key' => $token->getPermanentAuthenticationKey(),
                ]);

                /** @var MindboxToken $currToken */
                $currToken = $this->getTokenStorage()->getToken();
                $currToken->setSessionKey($token->getSessionKey());
                $this->getTokenStorage()->setToken($currToken);

                return parent::request($methodDescription, $options, true);
            } else {
                throw $e;
            }
        }
    }

    /**
     * @return object|\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    private function getTokenStorage()
    {
        return $this->container->get('security.token_storage');
    }

    /**
     * @return object|\ADW\MindboxBundle\Security\User\MindboxUserProvider
     */
    private function getUserProvider()
    {
        return $this->container->get('adw_mindbox.security.user_provider');
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}