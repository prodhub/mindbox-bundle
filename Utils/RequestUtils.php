<?php

namespace ADW\MindboxBundle\Utils;

use ADW\MindboxBundle\RestClient\ServiceDescription\AbstractMindboxServiceDescription;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Form\Exception\InvalidArgumentException;

/**
 * Class RequestUtils
 *
 * @package ADW\MindboxBundle\Utils
 * @author Artur Vesker
 */
class RequestUtils
{

    /**
     * @param RequestInterface $request
     * @param $key
     * @param $id
     * @param $sessionKey
     * @return RequestInterface
     */
    public static function withAuthentication(RequestInterface $request, $key, $id, $sessionKey)
    {
        $modify['set_headers'] = [
            'Authorization' => self::createAuthorizationHeader($key, $id, $sessionKey, null),
        ];

        return \GuzzleHttp\Psr7\modify_request($request, $modify);
    }

    /**
     * @param $key
     * @param $id
     * @param $sessionKey
     * @param string $authType
     * @param null|string $staffLogin
     * @param null|string $staffPassword
     * @return string
     */
    public static function createAuthorizationHeader(
        $key,
        $id,
        $sessionKey,
        $authType,
        $staffLogin = null,
        $staffPassword = null
    ) {
        switch ($authType) {
            case AbstractMindboxServiceDescription::AUTH_TYPE_CUSTOMER:
                return sprintf("DirectCrm key=\"%s\" customerId=\"%s\" sessionKey=\"%s\"", $key, $id, $sessionKey);
                break;
            case AbstractMindboxServiceDescription::AUTH_TYPE_ANONYMOUS:
                return sprintf('DirectCrm key="%s" customerId="" sessionKey=""', $key);
                break;
            case AbstractMindboxServiceDescription::AUTH_TYPE_STAFF:
                if (null == $staffLogin) {
                    throw new InvalidArgumentException(sprintf('You forgot define valid staffLogin!'));
                }
                if (null == $staffPassword) {
                    throw new InvalidArgumentException(sprintf('You forgot define valid staffPassword!'));
                }
                return sprintf("DirectCrm staffLogin=\"%s\" staffPassword=\"%s\"", $staffLogin, $staffPassword);
                break;
            default:
                throw new InvalidArgumentException(
                    sprintf('Authorization type "%s" not supported!', $authType)
                );
                break;
        }
    }
}