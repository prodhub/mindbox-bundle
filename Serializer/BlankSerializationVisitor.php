<?php

namespace ADW\MindboxBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Metadata\VirtualPropertyMetadata;
use JMS\Serializer\XmlSerializationVisitor;

class BlankSerializationVisitor extends XmlSerializationVisitor
{
    /**
     * {@inheritdoc}
     */
    public function visitProperty(PropertyMetadata $metadata, $object, Context $context)
    {
        $v = $metadata->getValue($object);
        if (null === $v && $context->shouldSerializeNull() === true
            && !$metadata instanceof VirtualPropertyMetadata
        ) {
            $metadata->setValue($object, '');
        }

        return parent::visitProperty($metadata, $object, $context);
    }
}