<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

class StreetContact extends ContactElement
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\Groups({"get", "save"})
     */
    protected $name;

    /**
     * @var ContactType
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\ContactType")
     * @Serialized\Groups({"get", "save"})
     */
    protected $type;
}