<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ContactElement
 *
 * @package ADW\MindboxBundle\Contact
 * @author Artur Vesker
 * @author Sergey Cherkesov
 */
class ContactElement
{
    /**
     * @var int
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\Groups({"get", "save"})
     */
    protected $id = '@empty';

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\Groups({"get"})
     */
    protected $name;

    /**
     * @var ContactType
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\ContactType")
     * @Serialized\Groups({"get"})
     */
    protected $type;

    /**
     * ContactElement constructor.
     */
    public function __construct()
    {
        $this->type = new ContactType();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ContactType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $id
     * @return ContactElement
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $name
     * @return ContactElement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param ContactType $type
     * @return ContactElement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}