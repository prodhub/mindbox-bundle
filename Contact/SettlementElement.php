<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

class SettlementElement extends ContactElement
{
    const MODE_QUICK = 'quick';
    const MODE_SELECT = 'select';
    const MODE_CUSTOM = 'custom';

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\Groups({"get", "save"})
     */
    protected $mode = self::MODE_SELECT;

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     * @return ContactElement
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }
}