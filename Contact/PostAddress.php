<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class PostAddress
 *
 * @package ADW\MindboxBundle\Contact
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("postAddress")
 */
class PostAddress
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("postIndex")
     * @Serialized\Groups({"get", "save"})
     */
    protected $postIndex;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("house")
     * @Serialized\Groups({"get", "save"})
     */
    protected $house;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("estate")
     * @Serialized\Groups({"get", "save"})
     */
    protected $estate;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("building")
     * @Serialized\Groups({"get", "save"})
     */
    protected $building;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("unit")
     * @Serialized\Groups({"get", "save"})
     */
    protected $unit;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("flat")
     * @Serialized\Groups({"get", "save"})
     */
    protected $flat;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("comments")
     * @Serialized\XmlElement(cdata=false)
     * @Serialized\Groups({"get", "save"})
     */
    protected $comments;


    /**
     * @var ContactElement
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\ContactElement")
     * @Serialized\Groups({"get", "save"})
     */
    protected $region;

    /**
     * @var ContactElement
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\ContactElement")
     * @Serialized\Groups({"get", "save"})
     */
    protected $district;

    /**
     * @var SettlementElement
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\SettlementElement")
     * @Serialized\Groups({"get", "save"})
     */
    protected $settlement;

    /**
     * @var StreetContact
     *
     * @Serialized\Type("ADW\MindboxBundle\Contact\StreetContact")
     * @Serialized\Groups({"get", "save"})
     */
    protected $street;

    /**
     * @return string
     */
    public function getPostIndex()
    {
        return $this->postIndex;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @return string
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return string
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * @return ContactElement
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return ContactElement
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return ContactElement
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * @return ContactElement
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $postIndex
     * @return PostAddress
     */
    public function setPostIndex($postIndex)
    {
        $this->postIndex = $postIndex;
        return $this;
    }

    /**
     * @param string $house
     * @return PostAddress
     */
    public function setHouse($house)
    {
        $this->house = $house;
        return $this;
    }

    /**
     * @param string $building
     * @return PostAddress
     */
    public function setBuilding($building)
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @param string $estate
     * @return PostAddress
     */
    public function setEstate($estate)
    {
        $this->estate = $estate;
        return $this;
    }

    /**
     * @param string $unit
     * @return PostAddress
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @param string $flat
     * @return PostAddress
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;
        return $this;
    }

    /**
     * @param ContactElement $region
     * @return PostAddress
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param ContactElement $district
     * @return PostAddress
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @param ContactElement $settlement
     * @return PostAddress
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;
        return $this;
    }

    /**
     * @param ContactElement $street
     * @return PostAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     * @return PostAddress
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }




}