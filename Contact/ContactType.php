<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ContactType
 *
 * @package ADW\MindboxBundle\Contact
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("type")
 */
class ContactType
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("id")
     * @Serialized\Groups({"get", "save"})
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("fullName")
     * @Serialized\Groups({"get"})
     */
    protected $fullName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("shortName")
     * @Serialized\Groups({"get"})
     */
    protected $shortName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\XmlAttribute()
     * @Serialized\SerializedName("optimalName")
     * @Serialized\Groups({"get"})
     */
    protected $optimalName;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function getOptimalName()
    {
        return $this->optimalName;
    }

    /**
     * @param string $id
     * @return ContactType
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $fullName
     * @return ContactType
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @param string $shortName
     * @return ContactType
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @param string $optimalName
     * @return ContactType
     */
    public function setOptimalName($optimalName)
    {
        $this->optimalName = $optimalName;

        return $this;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->fullName;
    }
}