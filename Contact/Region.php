<?php

namespace ADW\MindboxBundle\Contact;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Region
 *
 * @package ADW\MindboxBundle\Contact
 * @author Artur Vesker
 *
 * @Serialized\XmlRoot("region")
 */
class Region extends ContactElement
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\XmlAttribute()
     */
    protected $id;
}