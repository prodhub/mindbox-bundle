# Social networks integration (example for Facebook and Vkontakte)


###app/config/security.yml

    security:
        ...
        firewalls:
            ...
            main:
                ...
                oauth:
                    resource_owners:
                        facebook:           "/login/check-facebook"
                        vkontakte:          "/login/check-vkontakte"
                    login_path:        /login
                    use_forward:       false
                    failure_path:      /login
    
                    oauth_user_provider:
                        service: hwi_oauth.user.provider


###app/config/config.yml

    hwi_oauth:
        firewall_names: [main]
        resource_owners:
            facebook:
                type:                facebook
                client_id:           11111111
                client_secret:       344tgg54g45g4g45t3
            vkontakte:
                type:                vkontakte
                client_id:           2222222222222
                client_secret:       gklrjkljlekrjgioerjg


###app/config/routing.yml

    hwi_oauth_redirect:
        resource: "@HWIOAuthBundle/Resources/config/routing/redirect.xml"
        prefix:   /connect
    
    hwi_oauth_connect:
        resource: "@HWIOAuthBundle/Resources/config/routing/connect.xml"
        prefix:   /connect
    
    hwi_oauth_login:
        resource: "@HWIOAuthBundle/Resources/config/routing/login.xml"
        prefix:   /login
    
    facebook_login:
        path: /login/check-facebook
    
    vkontakte_login:
        path: /login/check-vkontakte